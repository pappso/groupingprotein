Source: grouping-protein
Section: science
Priority: optional
Maintainer: Olivier Langella <olivier.langella@u-psud.fr>
Homepage: http://pappso.inra.fr/bioinfo
Standards-Version: 3.9.4
Build-Depends: debhelper (>= 7),
               dh-python,
               python3,
               cmake (>= 2.6),
               qt6-base-dev,
               libpappsomspp-dev (>= @LIBPAPPSOMSPP_VERSION@)
Build-Conflicts: qt3-dev-tools



Package: grouping-protein
Architecture: any
Multi-Arch: no
Depends: ${misc:Depends},
         ${shlibs:Depends},
         ${python:Depends},
         libpappsomspp0  (>= @LIBPAPPSOMSPP_VERSION@)
Description: Proteomics grouping algorithm
 Grouping-protein implements a fast algorithm that can filter thousands of peptides
