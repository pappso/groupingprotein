import re
from Bio import SeqIO
from Bio.SeqUtils.ProtParam import ProteinAnalysis

import argparse
import sys
import os

## line to parse
## /usr/local/bin/diann --f /gorgone/pappso/moulon/raw/20240326_test_astral/240314_Demo_inrae_Col-1_125ng_DIA_180SPD_rep01.mzML --f /gorgone/pappso/moulon/raw/20240326_test_astral/240314_Demo_inrae_Col-1_125ng_DIA_180SPD_rep02.mzML --f /gorgone/pappso/moulon/raw/20240326_test_astral/240314_Demo_inrae_Col-1_125ng_DIA_180SPD_rep03.mzML --f /gorgone/pappso/moulon/raw/20240326_test_astral/240314_Demo_inrae_Col-1_250ng_DIA_180SPD_rep01.mzML --f /gorgone/pappso/moulon/raw/20240326_test_astral/240314_Demo_inrae_Col-1_250ng_DIA_180SPD_rep02.mzML --f /gorgone/pappso/moulon/raw/20240326_test_astral/240314_Demo_inrae_Col-1_250ng_DIA_180SPD_rep03.mzML --lib --threads 4 --verbose 1 --out /gorgone/pappso/moulon/users/thierry/20240329_test_astral/diann/report.tsv --qvalue 0.01 --matrices --out-lib /gorgone/pappso/moulon/users/thierry/20240329_test_astral/diann/lib_spectra_ara.tsv --gen-spec-lib --predictor --fasta /gorgone/pappso/moulon/database/Araport11_genes.201606.pep.fasta --fasta /gorgone/pappso/moulon/database/contaminants_standarts.fasta --fasta-search --min-fr-mz 200 --max-fr-mz 1800 --met-excision --cut K*,R* --missed-cleavages 1 --min-pep-len 7 --max-pep-len 30 --min-pr-mz 300 --max-pr-mz 1000 --min-pr-charge 1 --max-pr-charge 4 --unimod4 --var-mods 1 --var-mod UniMod:35,15.994915,M --var-mod UniMod:1,42.010565,*n --monitor-mod UniMod:1 --no-prot-inf --reanalyse --smart-profiling --direct-quant

Known_modifications = {
		"UniMod:4" : {"unimod": "UniMod:4", "mass":"57.0214614868", "amino_acid":"C"}
	}


def parseReportLog(reportlogFile):
	if os.path.exists(reportlogFile):
		inputLog = open(reportlogFile, "r")
		fastafile = []
		inputfile  = []
		mods = {}
		no_inf = False
		QValue = 0
		matrix_Qvalue = -1
		reportDecoy = False
		for line in inputLog:
			# print(line)
			if "--fasta" in line:
				print(line)
				for m in re.finditer(r"--f (\S+.mzML|.mzXML)", line):
					print(m.group(1))
					inputfile.append(m.group(1))
				for m in re.finditer(r"--fasta (\S+)", line):
					print(m.group(1))
					fastafile.append(m.group(1))
				for m in re.finditer(r"--var-mod (\S+)", line):
					print(m.group(1))
					tabmod = m.group(1).split(",")
					mod = {"unimod":tabmod[0], "mass_modification": tabmod[1], "amino_acid": tabmod[2]}
					mods[tabmod[0]] = mod
				for m in re.finditer("--(unimod\d+)", line):
					print(m.group(1))
					unimod = m.group(1).replace("u", "U")
					unimod = unimod.replace("m", "M")
					unimod = unimod[0:6]+":"+unimod[6:]
					print(unimod)
					if unimod in Known_modifications.keys():
						mods[unimod] = Known_modifications[unimod]
				if "--no-prot-inf" in line:
					no_inf = True
				for  m in re.finditer(r"--qvalue (\S+)", line):
					print(m.group(1))
					QValue = float(m.group(1))
				for m in re.finditer(r"--matrix-qvalue (\S+)", line):
					print(m.group(1))
					matrix_Qvalue = float(m.group(1))
				if "--report-decoys" in line:
					reportDecoy = True
		result = {"fastafile":fastafile, "inputfile":inputfile, "mods":mods, "no_inf":no_inf,"QValue":QValue, "no_inf":no_inf, "matrix_Qvalue":matrix_Qvalue, "reportDecoy":reportDecoy}
		return result
	else:
		return -1


#Defined command line
desc = "Process Diann result to peptide result. \
    The result can then be grouped by gp-grouping program."
command = argparse.ArgumentParser(prog='gp-read-diann', \
    description=desc, usage='%(prog)s [options] files')
command.add_argument('-q', '--Qvalue', default=0.01, type=float, \
    help='Minimum peptide qvalue threshold (default:0.01)')
command.add_argument('-p', '--PG.Qvalue', default=0.01, type=float, \
    help='Minimum protein group qvalue threshold (default:0.01)')
command.add_argument('-o', '--outfile', nargs="?", \
    type=argparse.FileType("w"), default=sys.stdout, \
    help='Save peptide result default:STDOUT')
command.add_argument('-f', '--fasta', default="", type=str, \
	help='The Fasta file which is used for DIA-NN processing for multiple')
command.add_argument('-c', '--contaminant_fasta', default="", type=str, \
	help='The Contaminant Fasta file which is used for DIA-NN processing')
command.add_argument('file', metavar='file', nargs='+', \
    help='diann report file in tsv')
command.add_argument('-l', '--reportLog', default="", type=str, \
    help='Diann log report files (can ber used to get partially the parameter of Diann processing)')
command.add_argument('-d', '--debug', default = 0, type=int, \
	help="set the debug levels, permit to create some log files to verifiy the processing, 0 = no log, 1 = log")
command.add_argument('-v', '--version', action='version', \
    version='%(prog)s ${GP_VERSION}')

#Read arguments of command line
args = command.parse_args()

param = None

if args.reportLog !="":
	param  = parseReportLog(args.reportLog)

# print("parsing database to match diann ids to fasta ids")
#
# if param is not None:
# 	fastafiles = param["fastafile"]
# 	for fasta in fastafile:
# 		if os.path.exists(fasta):
# 			with open("/gorgone/pappso/moulon/database/uniprotkb_S_cerevisiae_S288C_20241125.fasta") as handle:
# 				for record in SeqIO.parse(handle, "fasta"):
# 					print(record.id)
# 					record_id = re.sub("^[a-zA-Z]+\|", "", record.id)
# 					print(record_id)
# 					record_id = re.sub("\|[a-zA-Z0-9]*_[a-zA-Z0-9]*", "", record_id)
# 					print(record_id)
# 					prot_descr[record_id] = record.id + " " + record.description
#

# prot_descr = {}
#
# with open("/gorgone/pappso/moulon/database/uniprotkb_S_cerevisiae_S288C_20241125.fasta") as handle:
# 	for record in SeqIO.parse(handle, "fasta"):
# 		print(record.id)
# 		record_id = re.sub("^[a-zA-Z]+\|", "", record.id)
# 		print(record_id)
# 		record_id = re.sub("\|[a-zA-Z0-9]*_[a-zA-Z0-9]*", "", record_id)
# 		print(record_id)
# 		prot_descr[record_id] = record.id + " " + record.description
# with open("/gorgone/pappso/moulon/database/contaminants_standarts.fasta") as handle:
#     for record in SeqIO.parse(handle, "fasta"):
#         prot_descr[record.id] = record.id + " " + record.description
#
#
#
# uniprot = {}
# uniprotfile = open("uniprot.csv", "r")
# for line in uniprotfile:
# 	if "record_id" not in line:
# 		line = line.replace('"', "")
# 		tabline = line.strip().split("\t")
# 		id = f"(UniMod:{tabline[0]})"
# 		uniprot[id] = {"id":tabline[0], "title":tabline[1], "full_name":tabline[2], "mono_isotopique":tabline[3]}
# uniprotfile.close()
#
if args.debug == 1:
	print("creating pep_ids log files")
	pep_ids = open("pep_ids.tsv", "w")
	pep_ids.write("Modified.sequence\tSequence\tmods\n")
# modify to force only one file
file = open(args.file[0], "r")
samples = {}
for line in file:
	if "File.Name" not in line:
		tabline = line.split("\t")
		sample = tabline[1]
		previous_match = 0
		mods = []
		mods_string = []
		for match in re.finditer(r"(\(UniMod\:[0-9]*\))", tabline[13]):

			if match.span()[0] == 1:
				print(f"{match.group()} : {match.span()[0]} {tabline[13]} {tabline[13][match.span()[0]-1]}, {match.span()[1]-match.span()[0]}")
				amino_acid = tabline[13][match.span()[0]]
				amino_acid_number = match.span()[0]+1
			else:
				amino_acid = tabline[13][match.span()[0]-1]
				amino_acid_number = match.span()[0]-previous_match
			#print(match.group())
			#print(f'<mod aa="{amino_acid}" pos="{amino_acid_number}" mod="{uniprot[match.group()]["mono_isotopique"]}" />')
			if param is not None:
				if match.group() in param["mods"].keys():
					mono_isotopique = param["mods"][match.group()]["mass"]
				else:
					if match.group() == "(UniMod:4)":
						mono_isotopique=57.0214614868
					elif match.group() == "(UniMod:35)":
						mono_isotopique=15.9949102402
					elif match.group() == "(UniMod:1)":
						mono_isotopique=42.0105667114
			mods.append({"amino_acid":amino_acid, "amino_acid_number":amino_acid_number, "mono_isotopique":mono_isotopique})
			previous_match += match.span()[1]-match.span()[0]
			mods_string.append(f"{amino_acid}{amino_acid_number}:{mono_isotopique}")
		pep_ids.write(f'{tabline[13]}\t{tabline[14]}\t{" ".join(mods_string)}\n')
		if sample in samples.keys():
			samples[sample].append({"proteins.Ids":tabline[3], "proteins.names":tabline[4], "sequence_mods":tabline[13], "sequence":tabline[14], "charge":tabline[16], "Q.value":tabline[17], "mods":mods})
		else:
			samples[sample] = [{"proteins.Ids":tabline[3], "proteins.names":tabline[4], "sequence_mods":tabline[13], "sequence":tabline[14], "charge":tabline[16], "Q.value":tabline[17], "mods":mods}]

pep_ids.close()
#
# outputfile = open("outputfile.xml", "w")
# outputfile.write('<?xml version="1.0" encoding="utf-8" ?>\n')
# outputfile.write('<peptide_result>\n')
# outputfile.write('<filter evalue="0.01" />\n')
# for sample in samples:
# 	outputfile.write(f'<sample name="{sample}" file="{sample}">\n')
# 	scanid = 0
# 	modlines = ""
# 	for peptiz in samples[sample]:
# 		scanid = scanid+1
# 	#	print(f"scan {scanid} : {peptiz}")
# 		current_pep = peptiz
# 		prot = current_pep["proteins.Ids"].split(";")
# 		pep_mass = ProteinAnalysis(current_pep['sequence']).molecular_weight()
# 		if len(current_pep['mods']) != 0:
# 			for mod in current_pep['mods']:
# 				pep_mass += float(mod["mono_isotopique"])
# 		outputfile.write(f"<scan num=\"{scanid}\" z=\"{current_pep['charge']}\" mhObs=\"{pep_mass}\">\n")
# 		for i in range(len(prot)):
# 			if len(current_pep['mods']) == 0:
# 				pep_mass = ProteinAnalysis(current_pep['sequence']).molecular_weight()
# 				outputfile.write(f"<psm seq=\"{current_pep['sequence']}\" mhTheo=\"{pep_mass}\" evalue=\"{current_pep['Q.value']}\" prot=\"{prot_descr[prot[i]]}\"></psm>\n")
# 			else:
# 				outputfile.write(f"<psm seq=\"{current_pep['sequence']}\" mhTheo=\"{pep_mass}\" evalue=\"{current_pep['Q.value']}\" prot=\"{prot_descr[prot[i]]}\">\n")
# 				for mod in current_pep['mods']:
# 					outputfile.write(f'<mod aa="{mod["amino_acid"]}" pos="{mod["amino_acid_number"]}" mod="{mod["mono_isotopique"]}" />\n')
# 				outputfile.write("</psm>\n")
# 		outputfile.write("</scan>\n")
# 	outputfile.write("</sample>\n")
# outputfile.write("</peptide_result>\n")
# outputfile.close()

# print(parseReportLog("/gorgone/pappso/moulon/users/thierry/20240329_test_astral/diann/col-O_DIA_180SPD/report.log.txt"))
