#!/usr/bin/python3

import xml.sax


#Class Defined Sax Handler
class OutputProteinCountingHandler(xml.sax.ContentHandler):
    def __init__(self, outfile, specific):
        xml.sax.ContentHandler.__init__(self)
        self.outfile = outfile
        self.specific = specific
        self.prots = {}
        self.protIds = {}
        self.sampleIds = {}
        self.sampleOrder = []
        self.count = {}
        self.firstElement = True

    def startElement(self, name, attrs):
        if self.firstElement:
            self.firstElement = False
            if name != "grouping_protein":
                raise Exception("The input is not a gp result file")
        elif name == "sample":
            self.sampleIds[attrs.get('id')] = attrs.get('name')
            self.sampleOrder.append(attrs.get('id'))
        elif name == "protein":
            self.protIds[attrs.get('id')] = attrs.get('desc')
        elif name == "group":
            self.groupId = attrs.get('id')
            self.subgroup = []
            self.prots.clear()
            self.count.clear()
        elif name == "subgroup":
            sub = attrs.get('id')
            self.subgroup.append(sub)
            self.prots[sub] = str(attrs.get('protIds')).split()
        elif name == "peptide":
            self.subs = attrs.get('subgroupIds').split()
        elif name == "spectrum":
            self.addCount(attrs.get('sample'))

    def endElement(self, name):
        if name == "sample_list":
            self.outfile.write("Group" + "\t" + "SubGroup" + "\t" \
                + "Description" + "\t" + "Redondancy")
            for samp in self.sampleOrder:
                self.outfile.write("\t" + self.sampleIds[samp])
            del(self.sampleIds)
            self.outfile.write("\n")
        elif name == "group":
            for sub in self.subgroup:
                self.printSubGroup(sub)
        elif name == "subgroup_list":
            for sub in self.subgroup:
                self.count[sub] = {}
                for samp in self.sampleOrder:
                    self.count[sub][samp] = 0

    def addCount(self, sample):
        if self.specific and len(self.subs) == 1:
            for sub in self.subs:
                self.count[sub][sample] += 1
        elif self.specific == False:
            for sub in self.subs:
                self.count[sub][sample] += 1

    def printSubGroup(self, sub):
        self.outfile.write(self.groupId + "\t" + sub + "\t" + \
            self.protIds[self.prots[sub][0]] + "\t" + \
            str(len(self.prots[sub])))
        for samp in self.sampleOrder:
            self.outfile.write("\t" + str(self.count[sub][samp]))
        self.outfile.write("\n")


import argparse
import sys

#Defined command line
command = argparse.ArgumentParser(prog='gp-output-protein-counting', \
    description='Export protein counting on gp-grouping result.', \
    usage='%(prog)s [options]')
command.add_argument('-i', '--infile', nargs="?", \
    type=argparse.FileType("r"), default=sys.stdin, \
    help='Open gp-grouping result default:STDIN')
command.add_argument('-o', '--outfile', nargs="?", \
    type=argparse.FileType("w"), default=sys.stdout, \
    help='Save protein counting on default:STDOUT')
command.add_argument('--specific', default=False, action='store_true', \
    help='Count only specific peptide for each subgroup Default:all')
command.add_argument('-v', '--version', action='version', \
    version='%(prog)s ${GP_VERSION}')

#Read arguments of command line
args = command.parse_args()

#Parse the input with xml sax reader
parser = xml.sax.make_parser()
parser.setContentHandler(OutputProteinCountingHandler(args.outfile, \
    args.specific))
parser.parse(args.infile)
