#!/usr/bin/python3

import sys
sys.path.append("@CMAKE_INSTALL_PREFIX@/lib/groupingprotein")

import xml.sax
import re
from xmlstreamwriter import Writer


#Class Defined Sax Handler
class XtandemXmlHandler(xml.sax.ContentHandler):
    def __init__(self, writer, evalue, xtandem_missed_cleavage_bug_filter):
        xml.sax.ContentHandler.__init__(self)
        self.writer = writer
        self.evalue = evalue
        self.pattern = re.compile('(.*)\\:reversed$')
        self.firstElement = True
        self.xtandem_missed_cleavage_bug_filter = xtandem_missed_cleavage_bug_filter
        self.onPsm = False

    def startElement(self, name, attrs):
        if self.firstElement:
            self.firstElement = False
            if name != "bioml":
                raise Exception("The input is not a xtandem result file")
        elif name == "group" and attrs.get('type') == "model":
            if ( (float(attrs.get('expect')) <= self.evalue) or (str(attrs.get('expect')) == str(self.evalue))):
                scan = int(attrs.get('id'))
                mh = float(attrs.get('mh'))
                z = int(attrs.get('z'))
                writer.write_start_element("scan")
                writer.write_attribute("num", str(scan))
                writer.write_attribute("z", str(z))
                writer.write_attribute("mhObs", str(mh))
                self.valid = True
            else:
                self.valid = False
        elif name == "protein":
            self.onProtein = True
        elif name == "domain" and self.valid \
            and ( (float(attrs.get('expect')) <= self.evalue) or (str(attrs.get('expect')) == str(self.evalue))):
            seq = attrs.get('seq')
            mh = float(attrs.get('mh'))
            evalue = float(attrs.get('expect'))
            nb_missed_cleavages = int(attrs.get('missed_cleavages'))
            if self.xtandem_missed_cleavage_bug_filter and len(seq) == (nb_missed_cleavages + 1):
                pass
            else:
                writer.write_start_element("psm")
                writer.write_attribute("seq", str(seq))
                writer.write_attribute("mhTheo", str(mh))
                writer.write_attribute("evalue", str(evalue))
                writer.write_attribute("prot", self.protein)
                self.pos = int(attrs.get('start'))
                self.onPsm = True
        elif name == "aa" and self.onPsm:
            _type = attrs.get('type')
            pos = int(attrs.get('at')) - self.pos + 1
            mod = float(attrs.get('modified'))
            writer.write_start_empty_element("mod")
            writer.write_attribute("aa", str(_type))
            writer.write_attribute("pos", str(pos))
            writer.write_attribute("mod", str(mod))
            if "pm" in attrs:
                writer.write_attribute("pm", attrs.get('pm'))
        self.data = ""

    def endElement(self, name):
        if name == "group":
            if self.valid:
                self.writer.write_end_element()  # scan
            self.valid = False
        elif name == "protein":
            self.onProtein = False
        elif name == "note" and self.onProtein:
            #modify accession to included reverse
            m = self.pattern.search(self.data)
            if m is not None:
                self.protein = m.group(1).replace(' ', '|reversed ', 1)
            else:
                self.protein = self.data
        elif name == "domain":
            if self.onPsm:
                self.writer.write_end_element()  # psm
            self.onPsm = False
        self.data = ""

    def characters(self, content):
        self.data += content

import argparse
import sys

#Defined command line
desc = "Process X!Tandem result to peptide result. \
    The result can then be grouped by gp-grouping program."
command = argparse.ArgumentParser(prog='gp-read-xtandem', \
    description=desc, usage='%(prog)s [options] files')
command.add_argument('-e', '--evalue', default=0.05, type=float, \
    help='Minimum peptide evalue threshold (default:0.05)')
command.add_argument('-b', '--bug', default=False, type=bool, \
    help='Circumvent X!Tandem bug in refine mode (default: False)')
command.add_argument('-o', '--outfile', nargs="?", \
    type=argparse.FileType("w"), default=sys.stdout, \
    help='Save peptide result default:STDOUT')
command.add_argument('files', metavar='files', nargs='+', \
    help='List of X!Tandem files to process')
command.add_argument('-v', '--version', action='version', \
    version='%(prog)s ${GP_VERSION}')

#Read arguments of command line
args = command.parse_args()

#Initialise the Output
writer = Writer(args.outfile)
writer.set_auto_formatting(False)
writer.write_start_document()
writer.write_start_element("peptide_result")
writer.write_start_empty_element("filter")
writer.write_attribute("evalue", str(args.evalue))

#Initialise message
message = sys.stderr

#Read n files with Sax parser
for _fileStr in args.files:
    _file = open(_fileStr,'r')
    message.write("Read files : " + _file.name + "\n")
    name = _file.name.split('/').pop().rstrip('.xml')
    writer.write_start_element("sample")
    writer.write_attribute("name", name)
    writer.write_attribute("file", _file.name)
    parser = xml.sax.make_parser()
    parser.setContentHandler(XtandemXmlHandler(writer, args.evalue, args.bug))
    parser.parse(_file)
    _file.close()
    writer.write_end_element()  # sample

writer.write_end_element()  # peptide_result
writer.write_end_document()
