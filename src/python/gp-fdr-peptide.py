#!/usr/bin/python3

import xml.sax
import re


class FdrPeptideHandler(xml.sax.ContentHandler):
    """Handler to parsed peptide result and count FDR"""
    def __init__(self, outfile, decoy):
        xml.sax.ContentHandler.__init__(self)
        self.outfile = outfile
        self.reverse = 0
        self.normal = 0
        self.pattern = re.compile(decoy)
        self.firstElement = True

    def startElement(self, name, attrs):
        if self.firstElement:
            self.firstElement = False
            if name != "peptide_result":
                raise Exception("The input is not a peptide result file")
        elif name == "scan":
            self.newScan = True
        elif name == "psm":
            if self.pattern.search(attrs.get('prot').split(' ')[0]) == None:
                self.normal += 1
            else:
                self.reverse += 1
            self.newScan = False

    def endDocument(self):
        self.outfile.write("Normal Peptide\t" + str(self.normal) + "\n")
        self.outfile.write("Decoy Peptide\t" + str(self.reverse) + "\n")
        fdr = float(self.reverse) / float(self.normal)
        fdr *= 100
        self.outfile.write("FDR %\t" + str(fdr) + "\n")

import argparse
import sys

#Defined command line
desc = 'Calculate peptide FDR on peptide result. \
    Use gp-read-... script first to process search engine result.'
command = argparse.ArgumentParser(prog='gp-fdr-peptide', \
    description=desc, usage='%(prog)s [options]')
command.add_argument('--decoy', nargs="?", type=str, \
    default="reversed", \
    help='Accession flag to indicate decoy proteins default:reversed')
command.add_argument('-i', '--infile', nargs="?", \
    type=argparse.FileType("r"), default=sys.stdin, \
    help='Open peptide result default:STDIN')
command.add_argument('-o', '--outfile', nargs="?", \
    type=argparse.FileType("w"), default=sys.stdout, \
    help='Save result of traitement default:STDOUT')
command.add_argument('-v', '--version', action='version', \
    version='%(prog)s ${GP_VERSION}')

#Read arguments of command line
args = command.parse_args()
sys.stderr.write("Flag of decoy is :'" + args.decoy + "'\n")

#Parse the input with xml sax reader
parser = xml.sax.make_parser()
parser.setContentHandler(FdrPeptideHandler(args.outfile, args.decoy))
parser.parse(args.infile)
