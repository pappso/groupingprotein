#!/usr/bin/python3

import sys
sys.path.append("@CMAKE_INSTALL_PREFIX@/lib/groupingprotein")

import xml.sax
import re
from xmlstreamwriter import Writer

def getPsimodAccession(mass_delta):
  min_range = 0.01
  delta_tmp = (mass_delta - 15.99)
  if ((delta_tmp > -min_range) and (delta_tmp < min_range)):
    return "MOD:00719"
  delta_tmp = (mass_delta - 57.02)
  if ((delta_tmp > -min_range) and (delta_tmp < min_range)):
    return "MOD:00397"
  delta_tmp = (mass_delta - 42.01)
  if ((delta_tmp > -min_range) and (delta_tmp < min_range)):
    return "MOD:00408"
  delta_tmp = (mass_delta - 79.966)
  if ((delta_tmp > -min_range) and (delta_tmp < min_range)):
    return "MOD:00696"
  delta_tmp = (mass_delta + 17.026)
  if ((delta_tmp > -min_range) and (delta_tmp < min_range)):
    return "MOD:01160"
  delta_tmp = (mass_delta + 18.01)
  if ((delta_tmp > -min_range) and (delta_tmp < min_range)):
    return "MOD:00704"
  delta_tmp = (mass_delta - 28.031300)
  if ((delta_tmp > -min_range) and (delta_tmp < min_range)):
    return "MOD:00429"
  delta_tmp = (mass_delta - 32.056407)
  if ((delta_tmp > -min_range) and (delta_tmp < min_range)):
    return "MOD:00552"
  delta_tmp = (mass_delta - 36.075670)
  if ((delta_tmp > -min_range) and (delta_tmp < min_range)):
    return "MOD:00638"
  else:
    return "notfound"

#Class Defined Sax Handler
class OutputMasschroqmlHandler(xml.sax.ContentHandler):
    groupId = "G01"
    alignId = "ms2_1"
    quantId = "quant_1"

    def __init__(self, writer):
        xml.sax.ContentHandler.__init__(self)
        self.writer = writer
        self.prots = {}
        self.sampleIds = []
        self.bestSeqMods = []
        self.protIdsTopep = ""
        self.writer.write_start_document()
        self.writer.write_start_element("masschroq")
        self.firstElement = True

    def startElement(self, name, attrs):
        if self.firstElement:
            self.firstElement = False
            if name != "grouping_protein":
                raise Exception("The input is not a gp result file")
        elif name == "sample_list":
            self.writer.write_start_element("rawdata")
        elif name == "sample":
            self.sampleIds.append(attrs.get('id'))
            self.writer.write_start_empty_element("data_file")
            self.writer.write_attribute("id", attrs.get('id'))
            self.writer.write_attribute("format", "mzxml")
            self.writer.write_attribute("path", attrs.get('name') + ".mzXML")
            self.writer.write_attribute("type", "profile")
        elif name == "protein_list":
            self.writer.write_start_element("protein_list")
        elif name == "protein":
            self.writer.write_start_empty_element("protein")
            self.writer.write_attribute("id", attrs.get('id'))
            self.writer.write_attribute("desc", attrs.get('desc'))
        elif name == "group_list":
            self.writer.write_start_element("peptide_list")
        elif name == "group":
            self.prots.clear()
        elif name == "subgroup":
            sub = attrs.get('id')
            self.prots[sub] = str(attrs.get('protIds')).split()
        elif name == "peptide":
            self.bestSeqMods = []
            self.writer.write_start_element("peptide")
            self.writer.write_attribute("id", attrs.get('id'))
            self.writer.write_attribute("mh", attrs.get('mhTheo'))
            subs = attrs.get('subgroupIds').split()
            ids = ""
            for sub in subs:
                ids += ' ' + str(' ').join(self.prots[sub])
            self.protIdsTopep = ids.lstrip()
        elif name == "sequence_modifs":
            count = int(attrs.get('count'))
            if len(self.bestSeqMods) == 0:
                self.bestSeqMods.append(count)
                self.bestSeqMods.append(attrs.get('seq'))
                self.bestSeqMods.append(attrs.get('mods'))
            elif self.bestSeqMods[0] < count:
                self.bestSeqMods[0] = count
                self.bestSeqMods[1] = attrs.get('seq')
                self.bestSeqMods[2] = attrs.get('mods')
        elif name == "spectrum":
            self.writer.write_start_empty_element("observed_in")
            self.writer.write_attribute("data", attrs.get('sample'))
            self.writer.write_attribute("scan", attrs.get('scan'))
            self.writer.write_attribute("z", attrs.get('charge'))

    def endElement(self, name):
        if name == "sample_list":
            self.writer.write_end_element()  # rawdata
            self.writer.write_start_element("groups")
            self.writer.write_start_empty_element("group")
            self.writer.write_attribute("id", self.groupId)
            self.writer.write_attribute("data_ids", \
                str(' ').join(self.sampleIds))
            self.writer.write_end_element()  # groups
        elif name == "protein_list":
            self.writer.write_end_element()  # protein_list
        elif name == "group_list":
            self.writer.write_end_element()  # peptide_list
            self.__printMethods()  # print methods
            self.writer.write_end_document()  # close all remaining end element
        elif name == "peptide":
            self.writer.write_end_element()  # peptide
        elif name == "sequence_modifs_list":  # Complete peptide element
            arr_mods = self.bestSeqMods[2].split()
            for new_modstr in arr_mods:
                if new_modstr.startswith( "pm@" ):
                    arr_onemod = new_modstr.split(":")
                    position_pm = int(arr_onemod[0][3:])-1
                    arr_point_mutation = arr_onemod[1].split("=>")
                    seq_mods = list(self.bestSeqMods[1])
                    seq_mods[position_pm] = arr_point_mutation[1]
                    self.bestSeqMods[1] = ''.join(seq_mods)
            self.writer.write_attribute("seq", self.bestSeqMods[1])
            self.writer.write_attribute("mods", self.bestSeqMods[2])
            self.writer.write_attribute("prot_ids", self.protIdsTopep)
            #seq="ELGVPIVMHDYLTGGFTANTTLSHYCR" mods="C26:57.02146 M8:15.99491 E1:-18.01056 pm@5:N=>T"
            arr_mods = self.bestSeqMods[2].split()
            self.writer.write_start_element("modifications")
            for new_modstr in arr_mods:
                if not new_modstr.startswith( "pm@" ):
                    arr_onemod = new_modstr.split(":")
                    self.writer.write_start_empty_element("psimod")
                    self.writer.write_attribute("at", str(int(arr_onemod[0][1:])))
                    self.writer.write_attribute("acc", getPsimodAccession(float(arr_onemod[1])))
            self.writer.write_end_element()

    def __printMethods(self):
        self.__printAlignmentMethod()
        self.__printQuantiMethod()
        self.__printQuantification()

    def __printAlignmentMethod(self):
        self.writer.write_start_element("alignments")
        self.writer.write_start_element("alignment_methods")
        self.writer.write_start_element("alignment_method")
        self.writer.write_attribute("id", self.alignId)
        self.writer.write_start_element("ms2")
        self.writer.write_start_element("ms2_tendency_halfwindow")
        self.writer.write_character("10")
        self.writer.write_end_element()  # ms2_tendency_halfwindow
        self.writer.write_start_element("ms2_smoothing_halfwindow")
        self.writer.write_character("15")
        self.writer.write_end_element()  # ms2_smoothing_halfwindow
        self.writer.write_start_element("ms1_smoothing_halfwindow")
        self.writer.write_character("0")
        self.writer.write_end_element()  # ms1_smoothing_halfwindow
        self.writer.write_end_element()  # ms2
        self.writer.write_end_element()  # alignment_method
        self.writer.write_end_element()  # alignment_methods
        self.writer.write_start_empty_element("align")
        self.writer.write_attribute("group_id", self.groupId)
        self.writer.write_attribute("method_id", self.alignId)
        self.writer.write_attribute("reference_data_id", self.sampleIds[0])
        self.writer.write_end_element()  # alignments

    def __printQuantiMethod(self):
        self.writer.write_start_element("quantification_methods")
        self.writer.write_start_element("quantification_method")
        self.writer.write_attribute("id", self.quantId)
        self.writer.write_start_element("xic_extraction")
        self.writer.write_attribute("xic_type", "max")
        self.writer.write_start_empty_element("ppm_range")
        self.writer.write_attribute("min", "10")
        self.writer.write_attribute("max", "10")
        self.writer.write_end_element()  # xic_extraction
        self.writer.write_start_element("xic_filters")
        self.writer.write_start_empty_element("anti_spike")
        self.writer.write_attribute("half", "5")
        self.writer.write_end_element()  # xic_filters
        self.writer.write_start_element("peak_detection")
        self.writer.write_start_element("detection_zivy")
        self.writer.write_start_element("mean_filter_half_edge")
        self.writer.write_character("1")
        self.writer.write_end_element()  # mean_filter_half_edge
        self.writer.write_start_element("minmax_half_edge")
        self.writer.write_character("4")
        self.writer.write_end_element()  # minmax_half_edge
        self.writer.write_start_element("maxmin_half_edge")
        self.writer.write_character("2")
        self.writer.write_end_element()  # maxmin_half_edge
        self.writer.write_start_element("detection_threshold_on_max")
        self.writer.write_character("5000")
        self.writer.write_end_element()  # detection_threshold_on_max
        self.writer.write_start_element("detection_threshold_on_min")
        self.writer.write_character("3000")
        self.writer.write_end_element()  # detection_threshold_on_min
        self.writer.write_end_element()  # detection_zivy
        self.writer.write_end_element()  # peak_detection
        self.writer.write_end_element()  # quantification_method
        self.writer.write_end_element()  # quantification_methods

    def __printQuantification(self):
        self.writer.write_start_element("quantification")
        self.writer.write_start_element("quantification_results")
        self.writer.write_start_empty_element("quantification_result")
        self.writer.write_attribute("output_file", "XIC_result")
        self.writer.write_attribute("format", "tsv")
        self.writer.write_start_empty_element("quantification_result")
        self.writer.write_attribute("output_file", "results")
        self.writer.write_attribute("format", "masschroqml")
        self.writer.write_end_element()  # quantification_results
        self.writer.write_start_element("quantify")
        self.writer.write_attribute("id", "q1")
        self.writer.write_attribute("withingroup", self.groupId)
        self.writer.write_attribute("quantification_method_id ", self.quantId)
        self.writer.write_start_empty_element("peptides_in_peptide_list")
        self.writer.write_attribute("mode", "post_matching")
        self.writer.write_end_element()  # quantify
        self.writer.write_end_element()  # quantification

import argparse
import sys

#Defined command line
command = argparse.ArgumentParser(prog='gp-output-masschroqml', \
    description='Export masschroqML on gp-grouping result.', \
    usage='%(prog)s [options]')
command.add_argument('-i', '--infile', nargs="?", \
    type=argparse.FileType("r"), default=sys.stdin, \
    help='Open gp-grouping result default:STDIN')
command.add_argument('-o', '--outfile', nargs="?", \
    type=argparse.FileType("w"), default=sys.stdout, \
    help='Save masschroqML on default:STDOUT')
command.add_argument('-v', '--version', action='version', \
    version='%(prog)s ${GP_VERSION}')

#Read arguments of command line
args = command.parse_args()

#Initialise the Output
writer = Writer(args.outfile)
writer.set_auto_formatting_indent("  ")

#Parse the input with xml sax reader
parser = xml.sax.make_parser()
parser.setContentHandler(OutputMasschroqmlHandler(writer))
parser.parse(args.infile)
