"""Module to write XML output on stream

xmlstreamwriter - A library miming Qt XMLStreamWriter
API for incrementally writing XML.
No namespace support.
"""

# Copyright (c) 2013 Benoit Valot

__author__ = "Benoit Valot <valot@moulon.inra.fr>"
__version__ = "0.1"

import sys

sys.path.insert(0, '/usr/lib/groupingprotein/xmlstreamwriter')

from writer import Writer
from error import Error
