
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the grouping protein software.
 *
 *     grouping protein is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     grouping protein is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with grouping protein.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#include "groupingmonitor.h"

GroupingMonitor::GroupingMonitor()
{
  _p_out = new QTextStream(stderr, QIODeviceBase::WriteOnly);
}

GroupingMonitor::~GroupingMonitor()
{

  _p_out->flush();
  delete _p_out;
}

void
GroupingMonitor::startGrouping(std::size_t total_number_protein,
                               std::size_t total_number_peptide)
{
  _total_number_protein = total_number_protein;
  _current_protein      = 0;
  (*_p_out) << "start grouping " << _total_number_protein << " proteins and "
            << total_number_peptide << " peptides\n";
  _p_out->flush();
}
void
GroupingMonitor::groupingProtein()
{
  _current_protein++;
  (*_p_out) << "grouping protein " << _current_protein << " on "
            << _total_number_protein << "\n";
  _p_out->flush();
}
void
GroupingMonitor::startRemovingNonInformativeSubGroupsInAllGroups(
  std::size_t total_number_group)
{
  (*_p_out) << "removing non informative subgroups in all groups ("
            << total_number_group << ")\n";
  _total_group_count = total_number_group;
  _group_count       = 0;
  _p_out->flush();
}
void
GroupingMonitor::stopRemovingNonInformativeSubGroupsInAllGroups(
  std::size_t total_number_group)
{
  (*_p_out) << "removing non informative subgroups finished, remaining "
            << total_number_group << " groups\n";
  _p_out->flush();
}
void
GroupingMonitor::removingNonInformativeSubGroupsInGroup()
{
  _group_count++;
  (*_p_out) << "removing non informative on a single group (" << _group_count
            << " on " << _total_group_count << ")\n";
  _p_out->flush();
}
void
GroupingMonitor::startNumberingAllGroups(std::size_t total_number_group)
{
  (*_p_out) << "numbering " << total_number_group << " groups\n";
  _p_out->flush();
}
void
GroupingMonitor::stopGrouping()
{
  (*_p_out) << "grouping finished\n";
  _p_out->flush();
}
