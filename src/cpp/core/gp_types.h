/*
 *
 * MassChroQ: Mass Chromatogram Quantification software.
 * Copyright (C) 2010 Olivier Langella, Edlira Nano, Benoit Valot, Michel Zivy.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * \file gp_types.h
 * \date Mars 07, 2013
 * \author Benoit Valot
 * \brief This header contains all the type re-definitions and all
 * the global variables definitions used in groupingprotein.
 *
 * For configuration global variable definitions see config.h.cmake file.
 */

#include <QString>
#include <cmath>

#ifndef _GP_TYPES_H_
#define _GP_TYPES_H_ 1


/************ Typedefs **************************************************/

/** \var typedef double mcq_double
    \brief A type definition for doubles
*/
typedef double gp_double;

/** \var typedef float mcq_float
    \brief A type definition for floats
*/
typedef float gp_float;

/*********** Global variables definitions*********************************/

/** \def MHPLUS 1.007825
    \brief The (monoisotopic) mass of the H+ atom
*/
const gp_double MHPLUS(1.007825);

/** \def ONEMILLION 1000000
    \brief One million integer, why not.
*/
const gp_double ONEMILLION(1000000);


#endif /* _GPF_TYPES_H_ */
