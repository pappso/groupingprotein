/*
 * gp_engine.cpp
 *
 *  Created on: 7 mars 2013
 *      Author: valot
 */

#include "gp_engine.h"
#include "gp_error.h"
#include "../input/peptide_result_parser.h"
#include "../output/protein_grouping_result.h"
#include "filter/peptide_repro_filter.h"
#include "filter/peptide_by_prot_filter.h"
#include "filter/fasta_filter.h"
#include <QXmlSimpleReader>
#include <QXmlInputSource>
#include <QDebug>
#include "groupingmonitor.h"

GpEngine::GpEngine(GpParams *params) : _p_params(params)
{

  _p_monitor        = new GroupingMonitor();
  _p_grp_experiment = new pappso::GrpExperiment(_p_monitor);
}

GpEngine::~GpEngine()
{
  // delete protein pointer
  qDebug() << "Deleting GpEngine";
  delete _p_grp_experiment;

  std::map<QString, ProteinMatch *>::iterator proteinIt;
  ProteinMatch *p_protein;
  for(proteinIt = _proteinMatchList.begin();
      proteinIt != _proteinMatchList.end();
      ++proteinIt)
    {
      p_protein = proteinIt->second;
      delete(p_protein);
    }
  _proteinMatchList.clear();

  // delete peptide pointer
  std::map<QString, Peptide *>::iterator peptideIt;
  Peptide *p_peptide;
  for(peptideIt = _peptideList.begin(); peptideIt != _peptideList.end();
      ++peptideIt)
    {
      p_peptide = peptideIt->second;
      delete(p_peptide);
    }
  _peptideList.clear();

  // delete sample pointer
  std::map<QString, Sample *>::iterator SampleIt;
  Sample *p_sample;
  for(SampleIt = _sampleList.begin(); SampleIt != _sampleList.end(); ++SampleIt)
    {
      p_sample = SampleIt->second;
      delete(p_sample);
    }
  _sampleList.clear();
}
std::vector<const Peptide *>
GpEngine::getPeptideListInGroup(unsigned int group_number) const
{

  // qDebug() << "GpEngine::getPeptideListInGroup begin " << group_number;
  std::vector<const Peptide *> peptide_list;

  for(std::pair<QString, Peptide *> pair_peptide : _peptideList)
    {

      if(pair_peptide.second->getGrpPeptideSp().get() != nullptr)
        {
          // qDebug() << "GpEngine::getPeptideListInGroup
          // pair_peptide.second->getGrpPeptideSp().get() != nullptr";

          if(pair_peptide.second->getGrpPeptideSp().get()->getGroupNumber() ==
             group_number)
            {
              peptide_list.push_back(pair_peptide.second);
            }
        }
    }
  return peptide_list;
}

void
GpEngine::performedTraitment()
{

  if(_p_params->getOnlyCount())
    {
      std::cout << " count only" << std::endl;
    }
  this->readInputStream();
  this->filterResult();
  if(_p_params->getOnlyCount())
    {
      std::cout << _proteinMatchList.size() << " proteins" << std::endl;
      std::cout << _peptideList.size() << " peptides" << std::endl;
    }
  else
    {
      this->performedGrouping();
      this->writeXmlOutputResult();
    }
}

void
GpEngine::readInputStream()
{
  try
    {
      PeptideResultParser parser(this);
      std::cerr << "Begin reading data" << std::endl;
      QXmlSimpleReader simplereader;
      simplereader.setContentHandler(&parser);
      simplereader.setErrorHandler(&parser);

      QXmlInputSource xmlInputSource(_p_params->getInputStream().device());
      qDebug() << "simplereader.parse(xmlInputSource)";
      if(simplereader.parse(xmlInputSource))
        {
        }
      else
        {
          throw GpError(parser.errorString());
        }
    }
  catch(GpError &error)
    {
      throw GpError(
        QObject::tr("error reading input stream :\n").append(error.qwhat()));
    }
  catch(std::exception &error)
    {
      throw GpError(
        QObject::tr("error reading input stream :\n").append(error.what()));
    }
  std::cerr << "Finish loading data" << std::endl;
}

void
GpEngine::filterResult()
{
  // remove peptide not reproductibly identified in at least X samples
  try
    {
      PeptideReproFilter peptideFilter(this->_p_params);
      peptideFilter.filterResult(_peptideList, _proteinMatchList);
      // remove protein not identified with at least X reproductible peptides
      PeptideByProtFilter proteinFilter(this->_p_params);
      proteinFilter.filterResult(_peptideList, _proteinMatchList);
    }
  catch(std::exception &error)
    {
      throw GpError(
        QObject::tr("error filtering result :\n").append(error.what()));
    }
}

void
GpEngine::performedGrouping()
{
  std::cerr << "Begin Grouping " << _proteinMatchList.size() << " proteins "
            << _peptideList.size() << " peptides" << std::endl;
  QRegularExpression regexp_decoy = _p_params->getRegexpDecoy();
  if(regexp_decoy.pattern().isEmpty())
    {
      std::cerr << "decoy proteins are not removed" << std::endl;
    }
  else
    {
      std::cerr << "decoy proteins are removed from grouping based on this "
                   "regular expression : "
                << regexp_decoy.pattern().toStdString() << std::endl;
    }
  //".*\\|reversed$"

  std::map<QString, ProteinMatch *>::iterator proteinIt;
  ProteinMatch *p_protein;
  for(proteinIt = _proteinMatchList.begin();
      proteinIt != _proteinMatchList.end();
      ++proteinIt)
    {
      p_protein = proteinIt->second;

      if((!regexp_decoy.pattern().isEmpty()) &&
         (regexp_decoy.match(p_protein->getAccession()).hasMatch()))
        {
          // is decoy => the protein is ignored by grouping
        }
      else
        {
          p_protein->setGroupingExperiment(_p_grp_experiment);
        }
      // subgroups.addProteinMatch(p_protein);
    }
  std::cerr << "End subgroup creation : " << std::endl;

  // groups.groupingSubGroupSet(subgroups);

  // remove contaminants proteins
  FastaFilter fastaFilter(_p_params);
  fastaFilter.filterResult(this);

  _p_grp_experiment->startGrouping();

  std::cerr << "End group creation : " << std::endl;
}

void
GpEngine::writeXmlOutputResult()
{
  std::cerr << "Begin Export" << std::endl;
  ProteinGroupingResult writer(this->_p_params);
  writer.printGroupingResult(this);
  std::cerr << "Finish Export" << std::endl;
}

ProteinMatch *
GpEngine::getProteinMatchInstance(QString access, QString desc)
{
  std::map<QString, ProteinMatch *>::iterator it;
  it = this->_proteinMatchList.find(access);
  if(it != _proteinMatchList.end())
    {
      // qDebug() << "Protein already exist : " + access;
      return it->second;
    }
  else
    {
      ProteinMatch *match       = new ProteinMatch(access, desc);
      _proteinMatchList[access] = match;
      qDebug() << "New Protein : " + access;
      return match;
    }
}

Sample *
GpEngine::getSampleInstance(QString name)
{
  std::map<QString, Sample *>::iterator it;
  it = this->_sampleList.find(name);
  if(it != _sampleList.end())
    return it->second;
  else
    {
      Sample *samp      = new Sample(name);
      _sampleList[name] = samp;
      return samp;
    }
}

Peptide *
GpEngine::getPeptideInstance(QString seq, gp_double mh)
{
  std::map<QString, Peptide *>::iterator it;
  if(_p_params->getOnlyPeptideSequence())
    {
      mh = 0;
    }
  // round 2 digits before:
  gp_double mh_round = round(mh * (gp_double)100) / (gp_double)100;

  QString data =
    seq.replace("L", "I") + QString("|%1").arg((unsigned long)(mh_round * 10));
  it = this->_peptideList.find(data);
  if(it != _peptideList.end())
    {
      // qDebug() << "Peptide already exist : " + data;
      return it->second;
    }
  else
    {
      qDebug() << "New Peptide : " + data;
      Peptide *pep       = new Peptide(seq, mh);
      _peptideList[data] = pep;
      return pep;
    }
}
