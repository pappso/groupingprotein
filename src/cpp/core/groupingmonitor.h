
/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the grouping protein software.
 *
 *     grouping protein is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     grouping protein is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with grouping protein.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#ifndef GROUPINGMONITOR_H
#define GROUPINGMONITOR_H

#include <pappsomspp/grouping/grpexperiment.h>
#include <QTextStream>

class GroupingMonitor : public pappso::GrpGroupingMonitorInterface
{
  public:
  GroupingMonitor();
  ~GroupingMonitor();
  virtual void startGrouping(std::size_t total_number_protein,
                             std::size_t total_number_peptide);
  virtual void groupingProtein();
  virtual void startRemovingNonInformativeSubGroupsInAllGroups(
    std::size_t total_number_group);
  virtual void stopRemovingNonInformativeSubGroupsInAllGroups(
    std::size_t total_number_group);
  virtual void removingNonInformativeSubGroupsInGroup();
  virtual void startNumberingAllGroups(std::size_t total_number_group);
  virtual void stopGrouping();

  private:
  QTextStream *_p_out;
  std::size_t _total_number_protein;
  std::size_t _total_number_peptide;
  std::size_t _current_protein;
  std::size_t _total_group_count;
  std::size_t _group_count;
};

#endif // GROUPINGMONITOR_H
