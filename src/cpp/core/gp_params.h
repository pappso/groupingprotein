/*
 * gp_params.h
 *
 *  Created on: 7 mars 2013
 *      Author: valot
 */
#include <iostream>
#include <QStringList>
#include <QTextStream>
#include <QFile>
#include <QCoreApplication>
#include <QRegularExpression>
#include "gp_error.h"

#pragma once

class GpParams
{
  public:
  GpParams(QCoreApplication *app);

  virtual ~GpParams();

  QTextStream &
  getInputStream() const
  {
    return *_p_in;
  }

  QFile *
  getOutputFile()
  {
    return _p_outfile;
  }

  bool
  isFastaFile()
  {
    return (_p_fasta != NULL);
  }

  QFile *
  getFastaFile()
  {
    if(_p_fasta == NULL)
      {
        throw new GpError(QString("--fasta file is NULL"));
      }
    return _p_fasta;
  }

  int
  getPepByProt() const
  {
    return _pepByProt;
  }

  int
  getPepRepro() const
  {
    return _pepRepro;
  }

  void setRegexpDecoy(QString decoy_pattern);
  QRegularExpression getRegexpDecoy() const;

  bool getOnlyCount() const;
  bool getOnlyPeptideSequence() const;

  private:
  void setInputStream(QString in);
  void setOuputStream(QString out);
  void setPepByProt(QString pepByProt);
  void setPepRepro(QString pepRepro);
  void parseArguments(QStringList &arg);
  void setFastaFile(QString fasta);

  int _pepByProt;
  int _pepRepro;
  QTextStream *_p_in;
  QRegularExpression _decoy_regexp;
  QFile *_p_outfile;
  QFile *_p_infile;
  QFile *_p_fasta;
  bool _only_count            = false;
  bool _only_peptide_sequence = false;
};
