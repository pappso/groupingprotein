/*
 * peptide_by_prot.h
 *
 *  Created on: 21 mars 2013
 *      Author: valot
 */

#ifndef PEPTIDE_BY_PROT_FILTER_H_
#define PEPTIDE_BY_PROT_FILTER_H_

#include "data_filter.h"

class PeptideByProtFilter : public DataFilter
{
  public:
  PeptideByProtFilter(GpParams *params);
  virtual ~PeptideByProtFilter();
  void filterResult(std::map<QString, Peptide *> &peptideList,
                    std::map<QString, ProteinMatch *> &proteinMatchList);
};

#endif /* PEPTIDE_BY_PROT_FILTER_H_ */
