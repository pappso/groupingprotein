/*
 * peptide.h
 *
 *  Created on: 8 mars 2013
 *      Author: valot
 */

#include <set>
#include <map>
#include "sample.h"
#include "../gp_types.h"
#include <pappsomspp/grouping/grpexperiment.h>

#pragma once

class Peptide
{
  public:
  Peptide(QString sequence, gp_double mh);
  virtual ~Peptide();

  const QString
  getPeptideName() const
  {
    return QString("%1%2").arg(_sequenceLi).arg(_mh);
  }

  gp_double
  getMh() const
  {
    return _mh;
  }

  const QString &
  getSequenceLi() const
  {
    return _sequenceLi;
  }

  const bool
  operator<(const Peptide &pep) const
  {
    if(_mh != pep._mh)
      return (_mh < pep._mh);
    else
      return (_sequenceLi < pep._sequenceLi);
  }

  void addSequencesModifs(QString sequence, QString modifs);

  unsigned int getTotalSpectra() const;

  void addHashSampleScan(Sample *sample, int scan, int charge);

  bool isReproductible(unsigned int &pepRepro) const;

  std::map<QString, int>::const_iterator
  getSequenceModifsBegin() const
  {
    return (_sequencesModifs.begin());
  }

  std::map<QString, int>::const_iterator
  getSequenceModifsEnd() const
  {
    return (_sequencesModifs.end());
  }

  std::map<Sample *, std::map<int, int>>::const_iterator
  getHashSampleScanBegin() const
  {
    return (_hashSampleScans.begin());
  }

  std::map<Sample *, std::map<int, int>>::const_iterator
  getHashSampleScanEnd() const
  {
    return (_hashSampleScans.end());
  }

  const std::map<Sample *, std::map<int, int>> &getHashSampleScanMap() const;

  void
  setGrpPeptideSp(pappso::GrpPeptideSp peptide_sp)
  {
    _peptide_sp = peptide_sp;
  }

  const pappso::GrpPeptideSp &getGrpPeptideSp() const;

  private:
  pappso::GrpPeptideSp _peptide_sp;
  const gp_double _mh;
  QString _sequenceLi;
  /* Sequence with conversion L to I*/
  std::map<QString, int> _sequencesModifs;
  /* Set of different sequences - modifications identified and counting*/
  std::map<Sample *, std::map<int, int>> _hashSampleScans;
  /* List of hashSampleScans (scan number, charge) identifying this peptides*/
};
