/*
 * peptide.cpp
 *
 *  Created on: 8 mars 2013
 *      Author: valot
 */

#include "peptide.h"

Peptide::Peptide(QString sequence, gp_double mh) : _mh(mh)
{
  this->_sequenceLi = sequence.replace("L", "I");
}

Peptide::~Peptide()
{
  _hashSampleScans.clear();
}

const pappso::GrpPeptideSp &
Peptide::getGrpPeptideSp() const
{
  return _peptide_sp;
}
void
Peptide::addSequencesModifs(QString sequence, QString modifs)
{
  QString seqmod = sequence + "|" + modifs;
  this->_sequencesModifs[seqmod] += 1;
}


void
Peptide::addHashSampleScan(Sample *sample, int scan, int charge)
{
  _hashSampleScans[sample].insert(std::pair<int, int>(scan, charge));
}

bool
Peptide::isReproductible(unsigned int &pepRepro) const
{
  if(_hashSampleScans.size() >= pepRepro)
    {
      return true;
    }
  else
    {
      return false;
    }
}
const std::map<Sample *, std::map<int, int>> &
Peptide::getHashSampleScanMap() const
{
  return _hashSampleScans;
}

unsigned int
Peptide::getTotalSpectra() const
{
  unsigned int total = 0;
  std::map<Sample *, std::map<int, int>>::const_iterator it;
  for(it = _hashSampleScans.begin(); it != _hashSampleScans.end(); it++)
    {
      total += it->second.size();
    }
  return (total);
}
