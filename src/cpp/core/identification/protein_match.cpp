/*
 * protein_match.cpp
 *
 *  Created on: 8 mars 2013
 *      Author: valot
 */

#include "protein_match.h"
#include <QStringList>
#include <iostream>

ProteinMatch::ProteinMatch(QString access, QString desc)
  : _accession(access), _description(desc)
{
}

ProteinMatch::~ProteinMatch()
{
}

void
ProteinMatch::addPeptides(Peptide *pep)
{
  _peptideSet.insert(pep);
}

void
ProteinMatch::removePeptide(std::set<Peptide *> &peptidesToRemove)
{
  _peptideSet.removeAll(peptidesToRemove);
}

unsigned int
ProteinMatch::getUniquePeptide() const
{
  std::set<QString> peptideSequence;

  for(std::set<Peptide *>::const_iterator peptideIt =
        _peptideSet.getList().begin();
      peptideIt != _peptideSet.getList().end();
      peptideIt++)
    {
      peptideSequence.insert((*peptideIt)->getSequenceLi());
    }
  return (peptideSequence.size());
}

void
ProteinMatch::writeFastaPeptides(QTextStream *p_out) const
{
  *p_out << ">" << this->getDescription() << Qt::endl;
  for(std::set<Peptide *>::const_iterator peptideIt =
        _peptideSet.getList().begin();
      peptideIt != _peptideSet.getList().end();
      peptideIt++)
    {
      *p_out << (*peptideIt)->getSequenceLi() << " ";
    }
  *p_out << Qt::endl;
  p_out->flush();
}

bool
ProteinMatch::hasCommonPeptideWith(const ProteinMatch &protMatch) const
{
  return this->_peptideSet.containsOnePeptideIn(protMatch._peptideSet);
}

const pappso::GrpProteinSp &
ProteinMatch::getGrpProteinSp() const
{
  return _sp_grp_protein;
}

void
ProteinMatch::setGroupingExperiment(pappso::GrpExperiment *_p_grp_experiment)
{

  _sp_grp_protein =
    _p_grp_experiment->getGrpProteinSp(_accession, _description);

  for(Peptide *peptide : _peptideSet.getList())
    {

      pappso::GrpPeptideSp peptide_sp = _p_grp_experiment->setGrpPeptide(
        _sp_grp_protein, peptide->getSequenceLi(), peptide->getMh());
      peptide->setGrpPeptideSp(peptide_sp);

      // add for count :
      for(auto &map_pair : peptide->getHashSampleScanMap())
        {
          for(size_t i = 1; i < map_pair.second.size(); i++)
            {
              _p_grp_experiment->setGrpPeptide(
                _sp_grp_protein, peptide->getSequenceLi(), peptide->getMh());
            }
        }
    }

  /*
          if (_protein_sp.get()->isContaminant()) {
              p_grp_experiment->addPostGroupingGrpProteinSpRemoval(_sp_grp_protein);
          }*/
}
