/*
 * sample.h
 *
 *  Created on: 8 mars 2013
 *      Author: valot
 */

#include <vector>
#include <QString>

#ifndef SAMPLE_H_
#define SAMPLE_H_

class Sample
{
  public:
  Sample(QString name);
  virtual ~Sample();

  const std::vector<QString> &
  getFiles() const
  {
    return _files;
  }

  void
  addFile(QString file)
  {
    _files.push_back(file);
  }

  const QString &
  getName() const
  {
    return _name;
  }

  const bool
  operator<(const Sample &samp) const
  {
    return (_name < samp._name);
  }

  private:
  QString _name;
  std::vector<QString> _files;
};

#endif /* SAMPLE_H_ */
