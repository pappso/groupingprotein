/**
 * \file output/proticdbml.cpp
 * \date 11/5/2017
 * \author Olivier Langella
 * \brief PROTICdbML writer
 */

/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of XTPcpp.
 *
 *     XTPcpp is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     XTPcpp is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with XTPcpp.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <olivier.langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "proticdbml.h"
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/utils.h>
#include <pappsomspp/grouping/grpprotein.h>
#include <pappsomspp/amino_acid/aa.h>


const QString
getXmlDouble(pappso::pappso_double number)
{
  return QString::number(number, 'g', 6);
}


const QString
getModifString(const pappso::Peptide &peptide)
{
  QStringList modif_list;
  unsigned int i = 1;
  for(const pappso::Aa &amino_acid : peptide)
    {
      std::vector<pappso::AaModificationP> aa_modif_list =
        amino_acid.getModificationList();
      QStringList aamodif;
      for(auto &&aa_modif : aa_modif_list)
        {
          if(!aa_modif->isInternal())
            {
              aamodif << QString::number(aa_modif->getMass(), 'f', 2);
            }
        }
      aamodif.sort();
      QString mod_str(aamodif.join("|"));
      if(!mod_str.isEmpty())
        {
          modif_list << QString("%1%2:%3")
                          .arg(i)
                          .arg(amino_acid.getLetter())
                          .arg(mod_str);
        }
      i++;
    }
  // return QString ("%1 %2").arg(modif_list.join("
  // ")).arg(this->toAbsoluteString());
  return modif_list.join(" ");
}


ProticdbMl::ProticdbMl(const QString &out_filename)
{
  //_p_digestion_pipeline = p_digestion_pipeline;

  //_mzidentml = "http://psidev.info/psi/pi/mzIdentML/1.1";
  QString complete_out_filename = out_filename;
  _output_file                  = new QFile(complete_out_filename);

  if(_output_file->open(QIODevice::WriteOnly))
    {
      _output_stream = new QXmlStreamWriter();
      _output_stream->setDevice(_output_file);
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("error : cannot open the PROTICdbML output file : %1\n")
          .arg(out_filename));
    }

  _output_stream->setAutoFormatting(true);
  _output_stream->writeStartDocument("1.0");
}

ProticdbMl::~ProticdbMl()
{
  delete _output_file;
  delete _output_stream;
}

void
ProticdbMl::close()
{
  _output_stream->writeEndDocument();
  _output_file->close();

  qDebug() << "ProticdbMl::close end duration = " << _duracel.elapsed() << "ms";
}


void
ProticdbMl::writeHeader()
{

  qDebug();
  _duracel.start();


  _output_stream->writeNamespace("http://www.w3.org/2001/XMLSchema-instance",
                                 "xsi");
  // writer.setDefaultNamespace(namespaceURI);
  _output_stream->writeStartElement("PROTICdb");
  //_output_stream->writeAttribute("xmlns","http://pappso.inra.fr/xsd/masschroqml/2.2");
  //_output_stream->writeAttribute("http://www.w3.org/2001/XMLSchema-instance","schemaLocation","http://pappso.inra.fr/xsd/masschroqml/2.2
  // http://pappso.inra.fr/xsd/masschroq-2.2.xsd");


  // writer.writeAttribute(xmlnsxsi, "noNamespaceSchemaLocation",
  // xsischemaLocation);

  _output_stream->writeAttribute("version", "1.0");

  _output_stream->writeAttribute("type", "MSidentificationResults");

  writeIdentMethod();

  _output_stream->writeStartElement("sequences");
  /*
  for(IdentificationGroup *p_ident_group :
      sp_project.get()->getIdentificationGroupList())
    {

      for(ProteinMatch *p_protein_match : p_ident_group->getProteinMatchList())
        {
          writeSequence(p_protein_match);
        }
    }
  //</sequences>
  _output_stream->writeEndElement();

  writeProject();
  */
}

void
ProticdbMl::writeSequence(const QString &id,
                          const QString &access,
                          const QString &desc,
                          const QString &seq)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  // not found

  pappso::Protein protein(desc, seq);
  _map_protid_mw.insert(std::pair<QString, double>(id, protein.getMass()));
  _output_stream->writeStartElement("sequence");
  _output_stream->writeAttribute("id", id);
  QString display_id = QString("%1 %2").arg(access).arg(desc);
  _output_stream->writeAttribute("display_id", display_id.left(60));
  // <dbxref key="AT5G16390"
  // dbname="AGI_LocusCode"></dbxref>
  // if (prot.get_dbxref_type().equals("no") == false) {
  /*
  for(const DbXref &dbxref :
      p_protein_match->getProteinXtpSp().get()->getDbxrefList())
    {
      _output_stream->writeStartElement("dbxref");
      _output_stream->writeAttribute("dbname",
                                     Utils::getDatabaseName(dbxref.database));
      _output_stream->writeAttribute("key", dbxref.accession);
      _output_stream->writeEndElement(); // dbxref
    }
    */
  _output_stream->writeStartElement("description");
  _output_stream->writeCharacters(desc);
  _output_stream->writeEndElement(); // description

  _output_stream->writeStartElement("seq");
  _output_stream->writeCharacters(seq);
  _output_stream->writeEndElement(); // seq

  _output_stream->writeEndElement(); // sequence

  _map_accession2xmlid.insert(std::pair<QString, QString>(access, id));
}


void
ProticdbMl::closeSequences()
{
  _output_stream->writeEndElement();
  _output_stream->writeStartElement("project");
  _output_stream->writeAttribute("name", "");
  _output_stream->writeAttribute("id", "p1");
  // ajout des echantillons_msrun
  // writeSamples();
  _output_stream->writeStartElement("samples");
}

void
ProticdbMl::closeSamples()
{
  _output_stream->writeEndElement(); //"samples");
  _output_stream->writeStartElement("msRuns");
}


void
ProticdbMl::closeMsRuns()
{
  _output_stream->writeEndElement(); //"msRuns");
  _output_stream->writeStartElement("identificationRuns");
  _output_stream->writeStartElement("identificationRun");
  // ajout d'une mnip d'identification
  _output_stream->writeAttribute("ident_method_id", "m1");
  _output_stream->writeAttribute("customdb_id", "customdb0");

  _output_stream->writeStartElement("description");
  _output_stream->writeStartElement("admin");
  // writer.writeStartElement("contact");
  // writer.writeStartElement("email");
  // writer.writeCharacters("valot@moulon.inra.fr");
  // writer.writeEndElement();// email

  // writer.writeStartElement("name");
  // writer.writeCharacters("Valot Benoit");
  // writer.writeEndElement();// name
  // writer.writeEndElement();// contact
  _output_stream->writeEndElement(); // admin

  // if we can retrieve original informations in xml xtandem results
  //
  // for(IdentificationDataSource *p_identification_data_source :
  //    p_identification->getIdentificationDataSourceList())
  //  {
  //    writeIdentificationDataSource(p_identification_data_source);
  //  }
}

void
ProticdbMl::closePeptideHits()
{
  _output_stream->writeEndElement(); //"peptideHits");
  _output_stream->writeStartElement("matchs");
}


void
ProticdbMl::closeMatchs()
{
  _output_stream->writeEndElement(); //"matchs");
  /*
  _output_stream->writeEndElement(); //"identificationRun");
  _output_stream->writeEndElement(); //"identificationRuns");
  _output_stream->writeEndElement(); //"project");
  _output_stream->writeEndElement(); //"PROTICdb");
  */
  _output_stream->writeEndDocument();
}

void
ProticdbMl::writeSample(const QString &sample_id, const QString &sample_name)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  m_sampleid_name.insert(std::pair<QString, QString>(sample_id, sample_name));
  //_output_stream->writeStartElement("samples");

  // for(MsRunSp msrun_sp : _sp_project.get()->getMsRunStore().getMsRunList())
  //  {
  _output_stream->writeStartElement("sample");
  // QString id_samp =
  //  "bsamp" + pappso::Utils::getLexicalOrderedString(_sample_to_id.size());
  // QString id_msrun = msrun_sp.get()->getXmlId();
  // QString name     = msrun_sp.get()->getSampleName();
  // balise sample
  _output_stream->writeAttribute("name", sample_name);
  _output_stream->writeAttribute("id", sample_id);
  _output_stream->writeEmptyElement("description");
  //_sample_to_id.insert(std::pair<QString, QString>(name, id_samp));

  // Element spectrumList = doc.createElement("spectrumList");
  // msRun.appendChild(spectrumList);

  // msrun_to_id.put(samp.getSampleName(), id_msrun);
  _output_stream->writeEndElement(); // sample
  // }

  //_output_stream->writeEndElement(); // samples
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}


void
ProticdbMl::writeMsRun(const QString &msrun_id,
                       const QString &sample_id,
                       const QString &sample_name,
                       const QString &filename)
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  //_output_stream->writeStartElement("msRuns");

  // for(MsRunSp msrun_sp : _sp_project.get()->getMsRunStore().getMsRunList())
  //  {

  // QString name = msrun_sp.get()->getSampleName();

  // QString id_samp = _sample_to_id.at(name);

  // Element spectrumList = doc.createElement("spectrumList");
  // msRun.appendChild(spectrumList);

  // QString id_msrun = msrun_sp.get()->getXmlId();

  // balise MsRun
  _output_stream->writeStartElement("msRun");
  _output_stream->writeAttribute("sample_id", sample_id);
  _output_stream->writeAttribute("id", msrun_id);

  _output_stream->writeStartElement("description");
  _output_stream->writeStartElement("admin");
  // writer.writeStartElement("contact");
  // writer.writeStartElement("email");
  // writer.writeCharacters("valot@moulon.inra.fr");
  // writer.writeEndElement();// email

  // writer.writeStartElement("name");
  // writer.writeCharacters("Valot Benoit");
  // writer.writeEndElement();// name
  // writer.writeEndElement();// contact

  _output_stream->writeStartElement("sourceFile");
  _output_stream->writeStartElement("nameOfFile");
  _output_stream->writeCharacters(filename);
  _output_stream->writeEndElement(); // nameOfFile
  _output_stream->writeEndElement(); // sourceFile

  _output_stream->writeStartElement("sampleName");
  _output_stream->writeCharacters(sample_name);
  _output_stream->writeEndElement(); // sampleName
  _output_stream->writeEndElement(); // admin
  _output_stream->writeEndElement(); // description

  _output_stream->writeEndElement(); // msRun
  //  }

  //_output_stream->writeEndElement(); // msRuns
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

void
ProticdbMl::writeIdentificationRunDesccriptionDataProcessingList(
  std::map<QString, QString> m_map_sampleid2filepath)
{
  for(auto pair_id_filepath : m_map_sampleid2filepath)
    {
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      XtandemSaxHandler *handler = new XtandemSaxHandler();
      _map_sampleid_data.insert(std::pair<QString, XtandemSaxHandler *>(
        pair_id_filepath.first, handler));

      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      QXmlSimpleReader simplereader;
      simplereader.setContentHandler(handler);
      simplereader.setErrorHandler(handler);

      QFile tandem_file(pair_id_filepath.second);

      if(!tandem_file.exists())
        {
          throw pappso::PappsoException(
            QString("tandem result file %1 does not exists")
              .arg(pair_id_filepath.second));
        }

      QXmlInputSource xmlInputSource(&tandem_file);

      if(simplereader.parse(xmlInputSource))
        {
        }
      else
        {
          throw pappso::PappsoException(handler->errorString());
        }

      _output_stream->writeStartElement("dataProcessing");
      _output_stream->writeStartElement("software");
      _output_stream->writeStartElement("name");
      // if(p_identification_data_source->getIdentificationEngine() ==
      //    IdentificationEngine::XTandem)
      // {
      _output_stream->writeAttribute("acc", "PROTICdbO:0000283");
      //}
      _output_stream->writeCharacters("X!Tandem");

      _output_stream->writeEndElement(); // name
      _output_stream->writeStartElement("version");
      _output_stream->writeCharacters(handler->version);
      _output_stream->writeEndElement(); // version
      _output_stream->writeEndElement(); // software
      _output_stream->writeStartElement("processingMethod");

      // mzXml source file name
      writeCvParam(
        "PROTICdbO:0000343", handler->mzfile, "MS/MS data source file name");
      // model file name
      writeCvParam("PROTICdbO:0000342",
                   handler->model_file,
                   "X!tandem xml model file name");
      // xtandem result file name
      writeCvParam("PROTICdbO:0000341",
                   pair_id_filepath.second,
                   "X!tandem xml result file name");


      _output_stream->writeEndElement(); // processingMethod
      _output_stream->writeEndElement(); // dataProcessing
      /*
      _output_stream->writeStartElement("dataProcessing");
      _output_stream->writeStartElement("software");
      _output_stream->writeStartElement("name");
      _output_stream->writeAttribute("acc", "PROTICdbO:0000283");
      _output_stream->writeCharacters("X!Tandem");
      _output_stream->writeEndElement();// name
      _output_stream->writeStartElement("version");
      _output_stream->writeCharacters(XtandemPipelineSession.getInstance()
                             .getConfig().getXtandemVersion());
      _output_stream->writeEndElement();// version
      _output_stream->writeEndElement();// software
      _output_stream->writeStartElement("processingMethod");
      _output_stream->writeEndElement();// processingMethod
      _output_stream->writeEndElement();// dataProcessing
      */
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
    }

  _output_stream->writeEndElement(); //</description>

  _output_stream->writeStartElement("peptideHits");
}

void
ProticdbMl::writeIdentMethod()
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  // Ajout des mÃ©thodes
  _output_stream->writeStartElement("identMeth");
  _output_stream->writeAttribute("name", "");
  _output_stream->writeAttribute("id", "m1");
  _output_stream->writeEndElement();

  // Ajout des base de donnÃ©es
  _output_stream->writeStartElement("customDb");
  _output_stream->writeAttribute("name", "");
  _output_stream->writeAttribute("id", "customdb0");
  _output_stream->writeEndElement();
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}


std::vector<PeptideHit>
ProticdbMl::findXtandemPeptideHit(const std::set<QString> &accession_set,
                                  const QString &sample_id,
                                  const QString &seqli,
                                  unsigned int scan,
                                  unsigned int charge)
{
  return (_map_sampleid_data.at(sample_id)->findXtandemPeptideHit(
    accession_set, scan, charge, seqli));
}
void
ProticdbMl::writePeptideHit(const std::set<QString> &accession_set,
                            const QString &grouping_id,
                            const QStringList &sg_list,
                            const QString &sample_id,
                            const QString &seqli,
                            unsigned int scan,
                            unsigned int charge)
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  m_count_peptide++;

  std::vector<PeptideHit> tandem_hit_list =
    findXtandemPeptideHit(accession_set, sample_id, seqli, scan, charge);

  if(tandem_hit_list.size() == 0)
    {
      // forget this peptide
      return;
      /*
    throw pappso::PappsoException(
      QObject::tr("error : tandem_hit_list.size() == 0 %1 %2 %3 %4\n")
        .arg(sample_id)
        .arg(seqli)
        .arg(scan)
        .arg(charge));
        */
    }

  QString ph_id = QString("ph%1").arg(m_count_peptide);
  for(auto sg_id : sg_list)
    {
      auto it = m_map_sg_sampleid.insert(
        std::pair<QString, std::set<QString>>(sg_id, std::set<QString>()));
      it.first->second.insert(sample_id);

      auto itbis =
        m_map_sg_phid.insert(std::pair<QString, std::vector<QString>>(
          sg_id, std::vector<QString>()));
      itbis.first->second.push_back(ph_id);
    }

  for(auto peptide_hit : tandem_hit_list)
    {
      ProticPeptideHitRef ref;
      // ref.peptide_hit_id = ph_id;
      ref.seq_id = _map_accession2xmlid[peptide_hit.accession];
      if(ref.seq_id.isEmpty())
        {
          throw pappso::PappsoException(
            QObject::tr("_map_accession2xmlid[%1] not found")
              .arg(peptide_hit.accession));
        }
      ref.residue_after_cter  = peptide_hit.residue_after_cter;
      ref.residue_before_nter = peptide_hit.residue_before_nter;
      ref.start               = peptide_hit.start;
      ref.stop                = peptide_hit.stop;
      auto it                 = m_map_phid_phref.insert(
        std::pair<QString, std::vector<ProticPeptideHitRef>>(
          ph_id, std::vector<ProticPeptideHitRef>()));
      it.first->second.push_back(ref);
    }

  PeptideHit &tandem_hit = tandem_hit_list.front();
  // param par default
  _output_stream->writeStartElement("peptideHit");
  _output_stream->writeAttribute(
    "calc_mr", getXmlDouble(tandem_hit.peptide.get()->getMass()));
  _output_stream->writeAttribute(
    "exp_mz",
    getXmlDouble((tandem_hit.exp_mass + (charge * pappso::MHPLUS)) / charge));
  _output_stream->writeAttribute(
    "delta",
    getXmlDouble(((tandem_hit.exp_mass + pappso::MHPLUS) -
                  tandem_hit.peptide.get()->getMz(1))));
  _output_stream->writeAttribute("exp_mr", getXmlDouble(tandem_hit.exp_mass));
  _output_stream->writeAttribute("acq_number",
                                 QString("%1").arg(tandem_hit.scan));
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  _output_stream->writeAttribute("ms_run_id", QString("msr%1").arg(sample_id));
  _output_stream->writeAttribute("id", ph_id);
  _output_stream->writeAttribute("exp_z", QString("%1").arg(charge));

  writeCvParam("PROTICdbO:0000339", grouping_id, "peptide mass id");
  /*
      if (xtpExperimentType.equals("phospho")) {
          this.writeCvParam("PROTICdbO:0000349",
                            Utils.getPappsoPhosphoPeptideMassId(group,
     peptideMass), "phosphopeptide mass id");
      }
  */

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  // cvparam specifique xtandem

  writeCvParam("PROTICdbO:0000287",
               getXmlDouble(tandem_hit.evalue),
               "xtandem peptide evalue");

  writeCvParam("PROTICdbO:0000288",
               getXmlDouble(tandem_hit.hyperscore),
               "xtandem peptide hyperscore");

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  writeCvParam("PROTICdbO:0000289",
               getXmlDouble(tandem_hit.rt),
               "xtandem peptide retention time");

  // sequences avec les modifs
  _output_stream->writeStartElement("pepSeq");
  _output_stream->writeStartElement("peptide");
  _output_stream->writeCharacters(tandem_hit.peptide.get()->getSequence());
  _output_stream->writeEndElement(); // peptide
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  if(!getModifString(*tandem_hit.peptide.get()).isEmpty())
    {
      writePtms(tandem_hit.peptide.get());
    }
  _output_stream->writeEndElement(); // pepSeq
  _output_stream->writeEndElement(); // peptideHit
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}


void
ProticdbMl::writePtms(pappso::Peptide *p_peptide)
{
  _output_stream->writeStartElement("ptms");
  QStringList modif_list;
  unsigned int i = 1;
  for(const pappso::Aa &amino_acid : *p_peptide)
    {
      std::vector<pappso::AaModificationP> aa_modif_list =
        amino_acid.getModificationList();
      QStringList aamodif;
      for(auto &&aa_modif : aa_modif_list)
        {
          if(!aa_modif->isInternal())
            {
              aamodif << QString::number(aa_modif->getMass(), 'f', 2);
              _output_stream->writeStartElement("ptm");
              _output_stream->writeAttribute("diff_mono",
                                             getXmlDouble(aa_modif->getMass()));
              _output_stream->writeAttribute("position", QString("%1").arg(i));
              _output_stream->writeAttribute("aa",
                                             QString(amino_acid.getLetter()));
              _output_stream->writeEndElement(); // ptm
            }
        }
      i++;
    }

  _output_stream->writeEndElement(); // ptms
}

void
ProticdbMl::writeMatch(const QString &subgroup_id,
                       const QStringList &protein_ids)
{
  try
    {
      // for (SubGroup sg : group.getSubGroupSet().getSubGroupList()) {
      auto it_sample_list = m_map_sg_sampleid.find(subgroup_id);
      if(it_sample_list == m_map_sg_sampleid.end())
        {
          // no sample, probably a bug, but we choose to ignore it
          return;
        }

      _output_stream->writeStartElement("match");

      std::set<QString> protein_in_subgroup;
      // samples
      for(QString sample_id : m_map_sg_sampleid.at(subgroup_id))
        {
          _output_stream->writeStartElement("matchSample");
          _output_stream->writeAttribute("sample_id", sample_id);
          _output_stream->writeAttribute("name", m_sampleid_name[sample_id]);
          _output_stream->writeEndElement(); // "matchSample");
        }

      // proteins
      for(const QString &prot_id : protein_ids)
        {
          protein_in_subgroup.insert(prot_id);
          _output_stream->writeStartElement("proteinHit");
          _output_stream->writeAttribute("sequence_id", prot_id);
          // proteinHit.setAttribute("score", "");
          _output_stream->writeAttribute("rank", "1");


          // //cvparam
          writeCvParam("PROTICdbO:0000284",
                       QString("%1").arg(_map_protid_mw[prot_id]),
                       "MW computation");
          // evalue
          /*
          writeCvParam("PROTICdbO:0000291",
                       Utils::getXmlDouble(p_protein_match->getLogEvalue()),
                       "Xtandem log evalue");

          // coverage
          writeCvParam("PROTICdbO:0000285",
                       Utils::getXmlDouble(p_protein_match->getCoverage()),
                       "protein coverage");

                       */

          // [Term]
          // id: PROTICdbO:0000337
          // name: protein group number
          writeCvParam("PROTICdbO:0000337", prot_id, "grouping number");

          _output_stream->writeEndElement(); // "proteinHit");
        }

      auto itsg = m_map_sg_phid.find(subgroup_id);
      if(itsg == m_map_sg_phid.end())
        {
          throw pappso::PappsoException(
            QObject::tr("subgroup_id %1 not found in m_map_sg_phid")
              .arg(subgroup_id));
        }
      for(auto phid : itsg->second)
        {

          _output_stream->writeStartElement("peptideHitRef");
          _output_stream->writeAttribute("peptide_hit_id", phid);

          // peptides
          auto itphref = m_map_phid_phref.find(phid);
          if(itphref == m_map_phid_phref.end())
            {
              throw pappso::PappsoException(
                QObject::tr("phid %1 not found in m_map_phid_phref").arg(phid));
            }
          for(auto phref : itphref->second)
            {
              // peptidesHitRef

              if(protein_in_subgroup.find(phref.seq_id) !=
                 protein_in_subgroup.end())
                {
                  _output_stream->writeStartElement("fromSeq");
                  _output_stream->writeAttribute("seq_id", phref.seq_id);
                  _output_stream->writeAttribute(
                    "start", QString("%1").arg(phref.start));
                  _output_stream->writeAttribute("stop",
                                                 QString("%1").arg(phref.stop));
                  _output_stream->writeAttribute("residue_before_nter",
                                                 phref.residue_before_nter);
                  _output_stream->writeAttribute("residue_after_cter",
                                                 phref.residue_after_cter);

                  _output_stream->writeEndElement(); // fromSeq
                }
            }
          _output_stream->writeEndElement(); // peptideHitRef
        }
      _output_stream->writeEndElement(); // "match");
    }
  catch(pappso::PappsoException &error)
    {
      throw pappso::PappsoException(
        QObject::tr("Error in ProticdbMl::writeMatch :\n%1")
          .arg(error.qwhat()));
    }
  catch(std::exception &error)
    {
      throw pappso::PappsoException(
        QObject::tr("Error in ProticdbMl::writeMatch stdex :\n%1")
          .arg(error.what()));
    }
}


void
ProticdbMl::writeOboModif(pappso::AaModificationP mod)
{
  _output_stream->writeStartElement("cvParam");
  _output_stream->writeAttribute("name", mod->getName());
  _output_stream->writeAttribute("cvLabel", "MOD");
  _output_stream->writeAttribute("accession", mod->getAccession());
  _output_stream->writeEndElement(); // cvParam
}

void
ProticdbMl::writeCvParam(QString acc, QString value, QString description)
{
  _output_stream->writeStartElement("cvParam");

  _output_stream->writeAttribute("value", value);
  if(description.isEmpty())
    {
      _output_stream->writeAttribute("name", "N.A.");
    }
  else
    {
      _output_stream->writeAttribute("name", description);
    }
  _output_stream->writeAttribute("cvLabel", "PROTICdbO");
  _output_stream->writeAttribute("accession", acc);
  _output_stream->writeEndElement(); // cvParam
}
