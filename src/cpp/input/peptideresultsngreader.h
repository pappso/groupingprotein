#pragma once

#include <QFileInfo>
#include <pappsomspp/processing/xml/xmlstreamreaderinterface.h>
#include "../core/gp_engine.h"
/**
 * @todo write docs
 */
class PeptideResultsNgReader : public pappso::XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  PeptideResultsNgReader();

  /**
   * Destructor
   */
  virtual ~PeptideResultsNgReader();

  const std::set<QString>& getProteinList() const;

  protected:
  virtual void readStream() override;


  private:
  bool m_isOk;

  std::set<QString> m_accessionList;
};
