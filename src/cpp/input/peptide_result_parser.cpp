/*
 * peptide_result_parser.cpp
 *
 *  Created on: 8 mars 2013
 *      Author: valot
 */

#include "peptide_result_parser.h"
#include <QDebug>

PeptideResultParser::PeptideResultParser(GpEngine *engine)
  : _engine(engine), _firstElement(true)
{
  qDebug() << "constructor";
}

PeptideResultParser::~PeptideResultParser()
{
}

bool
PeptideResultParser::startElement(const QString &namespaceURI,
                                  const QString &localName,
                                  const QString &qName,
                                  const QXmlAttributes &attributes)
{
  try
    {
      qDebug() << "qName";
      if(_firstElement)
        {
          _firstElement = false;
          if(qName != "peptide_result")
            throw new GpError(
              QString("The input is not a peptide result file"));
        }
      else if(qName == "sample")
        {
          _p_currentSample =
            _engine->getSampleInstance(attributes.value("name"));
          _p_currentSample->addFile(attributes.value("file"));
          qDebug() << "New Sample to read : " << _p_currentSample->getName();
        }
      else if(qName == "scan")
        {
          _currentScan   = attributes.value("num").toInt();
          _currentCharge = attributes.value("z").toInt();
        }
      else if(qName == "psm")
        {
          _currentMH       = attributes.value("mhTheo").toDouble();
          _currentSequence = attributes.value("seq");
          _sp_current_peptide =
            pappso::Peptide(_currentSequence).makeNoConstPeptideSp();
          QString desc      = attributes.value("prot");
          _p_currentProtein = _engine->getProteinMatchInstance(
            this->getAccessionTodesc(desc), desc);
        }
      else if(qName == "mod")
        {
          if(attributes.value("pm").isEmpty())
            { // not a point mutation
              //<mod aa="C" pos="18" mod="57.04" />
              _currentModifs += attributes.value("aa") +
                                attributes.value("pos") + ":" +
                                attributes.value("mod") + " ";
              pappso::AaModificationP modif =
                pappso::AaModification::getInstanceCustomizedMod(
                  attributes.value("mod").toDouble());
              _sp_current_peptide.get()->addAaModification(modif, 0);
            }
          else
            {
              _currentModifs += QString("pm@%1:%2=>%3 ")
                                  .arg(attributes.value("pos"))
                                  .arg(attributes.value("aa"))
                                  .arg(attributes.value("pm"));
            }
        }
    }
  catch(GpError &exception_pappso)
    {
      _errorStr = QObject::tr(
                    "ERROR in PeptideResultParser::startElement tag %1, PAPPSO "
                    "exception:\n%2")
                    .arg(qName)
                    .arg(exception_pappso.qwhat());
      return false;
    }
  catch(std::exception &exception_std)
    {
      _errorStr = QObject::tr(
                    "ERROR in PeptideResultParser::startElement tag %1, std "
                    "exception:\n%2")
                    .arg(qName)
                    .arg(exception_std.what());
      return false;
    }
  _currentText = "";
  return true;
}

bool
PeptideResultParser::endElement(const QString &namespaceURI,
                                const QString &localName,
                                const QString &qName)
{
  try
    {
      if(qName == "psm")
        {
          pappso::MzRange peptide_mh(
            _sp_current_peptide.get()->getMz(1),
            pappso::PrecisionFactory::getDaltonInstance(0.05));
          if(peptide_mh.contains(_currentMH))
            {
            }
          else
            {
              _errorStr = QObject::tr(
                            "error reading peptide %1 %2 in sample %3 scan %4 "
                            ":\ncurrentMH %5 != peptide mz %6\nmz input %5 is "
                            "different from the computed one %6... replacing "
                            "by computed mass, check your data")
                            .arg(_currentSequence)
                            .arg(_currentModifs)
                            .arg(_p_currentSample->getName())
                            .arg(_currentScan)
                            .arg(_currentMH)
                            .arg(peptide_mh.getMz());
              std::cerr << "WARNING" << std::endl
                        << _errorStr.toStdString().c_str() << std::endl;
              // throw new GpError( _errorStr);
              // return false;
            }
          _currentMH = peptide_mh.getMz();
          Peptide *p_currentPeptide =
            _engine->getPeptideInstance(_currentSequence, _currentMH);
          // complete le peptide et la proteines
          p_currentPeptide->addSequencesModifs(_currentSequence,
                                               _currentModifs.trimmed());
          p_currentPeptide->addHashSampleScan(
            _p_currentSample, _currentScan, _currentCharge);
          _p_currentProtein->addPeptides(p_currentPeptide);
          _currentSequence = "";
          _currentModifs   = "";
        }
    }
  catch(GpError &exception_pappso)
    {
      _errorStr = QObject::tr(
                    "ERROR in PeptideResultParser::startElement tag %1, PAPPSO "
                    "exception:\n%2")
                    .arg(qName)
                    .arg(exception_pappso.qwhat());
      return false;
    }
  catch(std::exception &exception_std)
    {
      _errorStr = QObject::tr(
                    "ERROR in PeptideResultParser::startElement tag %1, std "
                    "exception:\n%2")
                    .arg(qName)
                    .arg(exception_std.what());
      return false;
    }
  return true;
}

bool
PeptideResultParser::endDocument()
{
  return true;
}

bool
PeptideResultParser::startDocument()
{
  qDebug();
  return true;
}

bool
PeptideResultParser::characters(const QString &str)
{
  _currentText += str;
  return true;
}

bool
PeptideResultParser::fatalError(const QXmlParseException &exception)
{
  qDebug();
  _errorStr = QObject::tr(
                "Parse error at line %1, column %2:\n"
                "%3")
                .arg(exception.lineNumber())
                .arg(exception.columnNumber())
                .arg(exception.message());
  return false;
}

QString
PeptideResultParser::errorString() const
{
  qDebug();
  return _errorStr;
}

QString
PeptideResultParser::getAccessionTodesc(QString &desc)
{
  return desc.split(" ").at(0);
}
