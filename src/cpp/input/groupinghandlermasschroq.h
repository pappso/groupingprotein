
/**
 * \file src/cpp/input/groupinghandlermasschroq.h
 * \date 19/02/2025
 * \author Olivier Langella
 * \brief grouping xml results parser conversion 2 json for MassChroQ
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <pappsomspp/processing/xml/xmlstreamreaderinterface.h>
#include <pappsomspp/processing/uimonitor/uimonitorinterface.h>
#include <pappsomspp/processing/filters/filtersuitestring.h>
#include <pappsomspp/peptide/peptide.h>
#include <QFileInfo>
#include <QJsonObject>
#include <QJsonDocument>

/**
 * @todo write docs
 */
class GroupingHandlerMassChroq : public pappso::XmlStreamReaderInterface
{
  public:
  /**
   * Default constructor
   */
  GroupingHandlerMassChroq();

  /**
   * Destructor
   */
  virtual ~GroupingHandlerMassChroq();


  void populateJsonDocument(QJsonDocument &document) const;

  protected:
  virtual void readStream() override;

  private:
  void readParams();
  void readSampleList();
  void readProteinList();
  void readGroupList();

  const QJsonObject getJsonAlignmentMethod() const;
  const QJsonObject getJsonQuantificationMethod() const;

  void peptideAddOneMod(pappso::Peptide &peptide, const QString &one_mod) const;


  private:
  QJsonObject m_jsonMsRunList;
  QJsonObject m_jsonGroupList;
  QJsonObject m_jsonQuantifyGroup;
  QJsonObject m_jsonProteinList;
  QJsonObject m_jsonPeptideList;
  QJsonObject m_jsonMsRunPeptideList;
  std::map<QString, std::map<QString, QJsonArray>>
    m_msrunPeptideJsonObservationMap;
};
