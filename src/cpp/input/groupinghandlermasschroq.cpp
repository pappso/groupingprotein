
/**
 * \file src/cpp/input/groupinghandlermasschroq.cpp
 * \date 19/02/2025
 * \author Olivier Langella
 * \brief grouping xml results parser conversion 2 json for MassChroQ
 */

/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *
 * This file is part of MassChroQ.
 *
 *     MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "groupinghandlermasschroq.h"
#include <pappsomspp/utils.h>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/masschroq/alignmentmethod.h>
#include <pappsomspp/masschroq/quantificationmethod.h>
#include <QJsonArray>

GroupingHandlerMassChroq::GroupingHandlerMassChroq()
{
}

GroupingHandlerMassChroq::~GroupingHandlerMassChroq()
{
}

const QJsonObject
GroupingHandlerMassChroq::getJsonAlignmentMethod() const
{
  qDebug();
  pappso::masschroq::AlignmentMethod method("a1");

  qDebug();
  return method.getJsonObject();
}

const QJsonObject
GroupingHandlerMassChroq::getJsonQuantificationMethod() const
{
  qDebug();
  pappso::masschroq::QuantificationMethod method("q1");

  qDebug();
  return method.getJsonObject();
}


void
GroupingHandlerMassChroq::populateJsonDocument(QJsonDocument &document) const
{

  QJsonObject root;
  QJsonObject identification_data;


  identification_data.insert("msrun_list", m_jsonMsRunList);
  identification_data.insert("peptide_list", m_jsonPeptideList);
  identification_data.insert("msrunpeptide_list", m_jsonMsRunPeptideList);
  identification_data.insert("protein_list", m_jsonProteinList);
  // root.insert("project_parameters", getJsonProjectParameters());
  root.insert("identification_data", identification_data);

  qDebug();
  QJsonObject methods;
  methods.insert("alignment_method", getJsonAlignmentMethod());

  qDebug();
  methods.insert("quantification_method", getJsonQuantificationMethod());

  qDebug();
  root.insert("masschroq_methods", methods);

  qDebug();
  QJsonObject action;
  /*
   *
"action": {
    "match_between_run": true,
    "isotope_minimum_ratio": 0.9,
    "group_list": { "g1": ["msruna1","msruna2","msruna3"]
    },
    "quantify_group": {"g1": {
        "alignment_reference":  "msruna1"
    }}
}*/
  action.insert("group_list", m_jsonGroupList);
  action.insert("align_group", m_jsonQuantifyGroup);
  action.insert("quantify_all", "true");

  root.insert("actions", action);
  document.setObject(root);
  qDebug();
}


void
GroupingHandlerMassChroq::readStream()
{
  try
    {
      qDebug() << m_qxmlStreamReader.name().toString();
      if(m_qxmlStreamReader.readNextStartElement())
        {
          qDebug() << m_qxmlStreamReader.name().toString();
          if(m_qxmlStreamReader.name().toString() == "grouping_protein")
            {

              while(m_qxmlStreamReader.readNextStartElement())
                {
                  qDebug() << m_qxmlStreamReader.name();
                  if(m_qxmlStreamReader.name().toString() == "sample_list")
                    {
                      readSampleList();
                    }
                  else if(m_qxmlStreamReader.name().toString() == "params")
                    {
                      readParams();
                      // m_qxmlStreamReader.skipCurrentElement();
                    }
                  else if(m_qxmlStreamReader.name().toString() ==
                          "protein_list")
                    {
                      readProteinList();
                    }
                  else if(m_qxmlStreamReader.name().toString() == "group_list")
                    {
                      readGroupList();
                    }
                  else
                    {


                      m_qxmlStreamReader.raiseError(
                        QObject::tr("ERROR parsing XML file at tag "
                                    "grouping_protein:\n%1 unknown")
                          .arg(m_qxmlStreamReader.name()));
                    }
                }
            }
          else
            {
              m_qxmlStreamReader.raiseError(
                QObject::tr("ERROR not an XML grouping result"));
            }
        }
    }

  catch(const pappso::PappsoException &e)
    {
      m_qxmlStreamReader.raiseError(
        QObject::tr("ERROR parsing XML file at tag %1:\n%2")
          .arg(m_qxmlStreamReader.name())
          .arg(e.qwhat()));
    }
}

void
GroupingHandlerMassChroq::readProteinList()
{
  while(m_qxmlStreamReader.readNextStartElement())
    {
      QString id   = m_qxmlStreamReader.attributes().value("id").toString();
      QString desc = m_qxmlStreamReader.attributes().value("desc").toString();
      if(id.isEmpty())
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("the protein tag must have an id attribute."));
        }
      QJsonObject protein;
      protein.insert("description", desc);
      m_jsonProteinList.insert(id, protein);

      qDebug();
      m_qxmlStreamReader.skipCurrentElement();
    }
}

void
GroupingHandlerMassChroq::readGroupList()
{
  /*
      <group id="a1" number="1">
          <subgroup_list>
              <subgroup id="a1.a1" number="1" protIds="a1.a1.a1"/>
              <subgroup id="a1.a2" number="2" protIds="a1.a2.a1 a1.a2.a2
     a1.a2.a3"/> <subgroup id="a1.a3" number="3" protIds="a1.a3.a1"/>
          </subgroup_list>
          <peptide_list>
              <peptide id="pepa1a1" seqLI="AIADGSIIDIIR" mhTheo="1256.7209"
     subgroupIds="a1.a1"> <sequence_modifs_list> <sequence_modifs
     seq="AIADGSLLDLLR" mods="" count="8"/>
                  </sequence_modifs_list>*/

  while(m_qxmlStreamReader.readNextStartElement())
    {
      if(m_qxmlStreamReader.name().toString() == "group")
        {
          std::map<QString, QStringList> map_sg2prot;
          m_qxmlStreamReader.readNextStartElement();
          if(m_qxmlStreamReader.name().toString() == "subgroup_list")
            {
              while(m_qxmlStreamReader.readNextStartElement())
                {
                  //<subgroup id="a1.a1" number="1" protIds="a1.a1.a1"/>
                  map_sg2prot.insert(
                    {m_qxmlStreamReader.attributes().value("id").toString(),
                     m_qxmlStreamReader.attributes()
                       .value("protIds")
                       .toString()
                       .split(" ", Qt::SplitBehaviorFlags::SkipEmptyParts)});
                  m_qxmlStreamReader.skipCurrentElement();
                }
            }
          else
            {
              m_qxmlStreamReader.raiseError(
                QObject::tr("ERROR parsing XML file at tag "
                            "subgroup_list:\n%1 unknown")
                  .arg(m_qxmlStreamReader.name()));
            }
          m_qxmlStreamReader.readNextStartElement();
          if(m_qxmlStreamReader.name().toString() == "peptide_list")
            {
              /*
               *
        "pepa1a1": {
            "proforma": "SLTNDWEDHLAVK",
            "proteins": ["prota1a1"],
            "mods": "free text",*/
              while(m_qxmlStreamReader.readNextStartElement())
                {
                  if(m_qxmlStreamReader.name().toString() == "peptide")
                    {
                      //<peptide id="pepa1a1" seqLI="AIADGSIIDIIR"
                      // mhTheo="1256.7209" subgroupIds="a1.a1">
                      QStringList prot_list;
                      for(QString sg_id :
                          m_qxmlStreamReader.attributes()
                            .value("subgroupIds")
                            .toString()
                            .split(" ", Qt::SplitBehaviorFlags::SkipEmptyParts))
                        {
                          for(QString prot_id : map_sg2prot.at(sg_id))
                            {
                              prot_list << prot_id;
                            }
                        }
                      QString pep_id =
                        m_qxmlStreamReader.attributes().value("id").toString();
                      QStringList mods;

                      pappso::Peptide peptide(m_qxmlStreamReader.attributes()
                                                .value("seqLI")
                                                .toString());

                      QJsonObject json_peptide;

                      QJsonArray protein_array;
                      prot_list.removeDuplicates();
                      for(const QString &prot_id : prot_list)
                        {
                          protein_array.append(prot_id);
                        }
                      // proteins OK

                      m_qxmlStreamReader.readNextStartElement();
                      if(m_qxmlStreamReader.name().toString() ==
                         "sequence_modifs_list")
                        {
                          while(m_qxmlStreamReader.readNextStartElement())
                            {
                              if(m_qxmlStreamReader.name().toString() !=
                                 "sequence_modifs")
                                {
                                  m_qxmlStreamReader.raiseError(
                                    QObject::tr(
                                      "ERROR parsing XML file expecting "
                                      "sequence_modifs and got %1")
                                      .arg(m_qxmlStreamReader.name()));
                                }
                              //<sequence_modifs seq="QLVATGKPDSFGGPFLVPSYR"
                              // mods="Q1:-17.02655" count="4"/>
                              QString one_mod = m_qxmlStreamReader.attributes()
                                                  .value("mods")
                                                  .toString();
                              if(!one_mod.isEmpty())
                                {
                                  peptideAddOneMod(peptide, one_mod);
                                  mods << one_mod;
                                }
                              m_qxmlStreamReader.skipCurrentElement();
                            }
                          mods.removeDuplicates();
                        }
                      else
                        {
                          m_qxmlStreamReader.raiseError(
                            QObject::tr("ERROR parsing XML file expecting "
                                        "sequence_modifs_list and got %1")
                              .arg(m_qxmlStreamReader.name()));
                        }

                      m_qxmlStreamReader.readNextStartElement();
                      if(m_qxmlStreamReader.name().toString() == "spectra")
                        {
                          while(m_qxmlStreamReader.readNextStartElement())
                            {

                              if(m_qxmlStreamReader.name().toString() !=
                                 "spectrum")
                                {
                                  m_qxmlStreamReader.raiseError(
                                    QObject::tr(
                                      "ERROR parsing XML file expecting "
                                      "spectrum and got %1")
                                      .arg(m_qxmlStreamReader.name()));
                                }
                              //<spectrum sample="samp1" scan="14121"
                              // charge="2"/>
                              QJsonObject observation;
                              QString msrun_id = m_qxmlStreamReader.attributes()
                                                   .value("sample")
                                                   .toString();
                              QString scan = m_qxmlStreamReader.attributes()
                                               .value("scan")
                                               .toString();
                              QString charge = m_qxmlStreamReader.attributes()
                                                 .value("charge")
                                                 .toString();

                              observation.insert("scan", scan.toLongLong());

                              if(charge.isEmpty())
                                {
                                  m_qxmlStreamReader.raiseError(
                                    QObject::tr("the spectrum tag must have "
                                                "a charge attribute."));
                                }

                              QJsonObject precursor;

                              precursor.insert("charge", charge.toInt());

                              observation.insert("precursor", precursor);

                              /// add the observed in infos to the current
                              /// msunpeptide observed peptide
                              auto itmsrun =
                                m_msrunPeptideJsonObservationMap.insert(
                                  {msrun_id, std::map<QString, QJsonArray>()});
                              auto itpeparr = itmsrun.first->second.insert(
                                {pep_id, QJsonArray()});
                              itpeparr.first->second.append(observation);
                              m_qxmlStreamReader.skipCurrentElement();
                            }
                        }
                      else
                        {
                          m_qxmlStreamReader.raiseError(
                            QObject::tr("ERROR parsing XML file expecting "
                                        "spectra and got %1")
                              .arg(m_qxmlStreamReader.name()));
                        }

                      json_peptide.insert("proteins", protein_array);

                      json_peptide.insert("proforma", peptide.toProForma());
                      json_peptide.insert("mods", mods.join(" "));


                      m_jsonPeptideList.insert(pep_id, json_peptide);
                      m_qxmlStreamReader.skipCurrentElement();
                    }
                  else
                    {
                      m_qxmlStreamReader.raiseError(
                        QObject::tr("ERROR parsing XML file at tag "
                                    "peptide:\n%1 unknown")
                          .arg(m_qxmlStreamReader.name()));
                    }
                }
            }
          else
            {
              m_qxmlStreamReader.raiseError(
                QObject::tr("ERROR parsing XML file at tag "
                            "peptide_list:\n%1 unknown")
                  .arg(m_qxmlStreamReader.name()));
            }
          m_qxmlStreamReader.skipCurrentElement();
        }
      else
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("ERROR parsing XML file at tag "
                        "group_list:\n%1 unknown")
              .arg(m_qxmlStreamReader.name()));
        }
    }
  qDebug();
  //"msruna1": {
  //"peptide_obs": { "pepa1a1":
  //  [{

  for(auto &itmsrun_pair : m_msrunPeptideJsonObservationMap)
    {
      qDebug();
      QJsonObject peptide_list;
      QString msrun = itmsrun_pair.first;
      for(auto &itpeptide_pair : itmsrun_pair.second)
        {
          peptide_list.insert(itpeptide_pair.first, itpeptide_pair.second);
        }
      QJsonObject peptide_obs;
      peptide_obs.insert("peptide_obs", peptide_list);
      m_jsonMsRunPeptideList.insert(msrun, peptide_obs);
    }
  m_msrunPeptideJsonObservationMap.clear();
  qDebug();
}


void
GroupingHandlerMassChroq::readSampleList()
{
  QStringList msrun_id_list;
  while(m_qxmlStreamReader.readNextStartElement())
    {
      //<sample id="samp1" name="20120906_balliau_extract_1_A01_urnb-1">
      //  <xtandem
      //  file="/home/langella/data1/tandem/extraction/20120906_balliau_extract_1_A01_urnb-1.xml"/>
      //</sample>
      if(m_qxmlStreamReader.name().toString() == "sample")
        {

          QJsonObject msrun;

          msrun.insert(
            "file", m_qxmlStreamReader.attributes().value("name").toString());

          m_jsonMsRunList.insert(
            m_qxmlStreamReader.attributes().value("id").toString(), msrun);
          msrun_id_list
            << m_qxmlStreamReader.attributes().value("id").toString();
          m_qxmlStreamReader.skipCurrentElement();
        }
    }


  QJsonArray group;
  QString ref_msrun_id;
  for(QString id : msrun_id_list)
    {
      if(ref_msrun_id.isEmpty())
        ref_msrun_id = id;
      group.append(id);
    }

  m_jsonGroupList.insert("g1", group);


  if(!ref_msrun_id.isEmpty())
    {
      QJsonObject qgroup;
      qgroup.insert("alignment_reference", ref_msrun_id);
      m_jsonQuantifyGroup.insert("g1", qgroup);
    }
}

void
GroupingHandlerMassChroq::readParams()
{
  m_qxmlStreamReader.skipCurrentElement();
}

void
GroupingHandlerMassChroq::peptideAddOneMod(pappso::Peptide &peptide,
                                           const QString &one_mod) const
{
  //"E1:-18.01056"
  QStringList mod_split = one_mod.split(":");
  double mass           = mod_split.at(1).toDouble();
  pappso::AminoAcidChar aa =
    (pappso::AminoAcidChar)mod_split.at(0).at(0).toLatin1();
  std::size_t position = mod_split.at(0).mid(1).toInt() - 1;


  pappso::AaModificationP pmod =
    pappso::Utils::guessAaModificationPbyMonoisotopicMassDelta(aa, mass);

  peptide.addAaModification(pmod, position);
}
