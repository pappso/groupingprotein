#include "peptideresultsngreader.h"

#include <QRegularExpression>

PeptideResultsNgReader::PeptideResultsNgReader()
{
}
PeptideResultsNgReader::~PeptideResultsNgReader()
{
}


void
PeptideResultsNgReader::readStream()
{

  m_isOk = false;
  qDebug();
  if(m_qxmlStreamReader.readNextStartElement())
    {
      qDebug() << m_qxmlStreamReader.name();
      if(m_qxmlStreamReader.name().toString() == "peptide_result")
        {
          qDebug();
          while(m_qxmlStreamReader.readNextStartElement())
            {
              qDebug() << m_qxmlStreamReader.name();

              if(m_qxmlStreamReader.name().toString() == "filter")
                {
                  m_qxmlStreamReader.skipCurrentElement();
                }
              else if(m_qxmlStreamReader.name().toString() == "sample")
                {
                  // read_rawdata();

                  while(m_qxmlStreamReader.readNextStartElement())
                    {

                      if(m_qxmlStreamReader.name().toString() == "scan")
                        {

                          while(m_qxmlStreamReader.readNextStartElement())
                            {

                              if(m_qxmlStreamReader.name().toString() == "psm")
                                {
                                  // <psm
                                  // seq="TIKVSIVGSELGASLVEPQGADAAAITSVEIGSGLTGESCR"
                                  // mhTheo="4030.06530307" evalue="3.375680447"
                                  // prot="AS117:gene_id_29219  [gene]
                                  // locus=scaffold5827_2:644:1744:+[Complete]">
                                  qDebug()
                                    << m_qxmlStreamReader.attributes().value(
                                         "prot");
                                  QStringList prot_str =
                                    m_qxmlStreamReader.attributes()
                                      .value("prot")
                                      .toString()
                                      .split(QRegularExpression("\\s+"),
                                             Qt::SkipEmptyParts);
                                  if(prot_str.size() > 0)
                                    {
                                      auto it =
                                        m_accessionList.insert(prot_str.at(0));

                                      qDebug() << *it.first;
                                    }
                                  m_qxmlStreamReader.skipCurrentElement();
                                }
                              else
                                {
                                  m_qxmlStreamReader.raiseError(
                                    QObject::tr("Unexpected element %1 in scan")
                                      .arg(
                                        m_qxmlStreamReader.name().toString()));
                                  m_qxmlStreamReader.skipCurrentElement();
                                }
                            }
                        }
                      else
                        {
                          m_qxmlStreamReader.raiseError(
                            QObject::tr("Unexpected element %1 in sample")
                              .arg(m_qxmlStreamReader.name().toString()));
                          m_qxmlStreamReader.skipCurrentElement();
                        }
                    }
                }
              else
                {
                  m_qxmlStreamReader.raiseError(
                    QObject::tr("Unexpected element %1 in pepetide_result")
                      .arg(m_qxmlStreamReader.name().toString()));
                  m_qxmlStreamReader.skipCurrentElement();
                }
            }
        }
      else
        {
          m_qxmlStreamReader.raiseError(
            QObject::tr("Unexpected element %1")
              .arg(m_qxmlStreamReader.name().toString()));
          m_qxmlStreamReader.skipCurrentElement();
        }
    }

  if(errorString().isEmpty())
    {
      m_isOk = true;
    }
  qDebug();
}

const std::set<QString> &
PeptideResultsNgReader::getProteinList() const
{
  return m_accessionList;
}
