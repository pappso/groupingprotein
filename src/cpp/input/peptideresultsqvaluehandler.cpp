/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of Protein Grouper.
 *
 *     Protein Grouper is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Protein Grouper is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Protein Grouper.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "peptideresultsqvaluehandler.h"
#include <pappsomspp/pappsoexception.h>

PeptideResultsQvalueHandler::PeptideResultsQvalueHandler()
{

  m_regexpDecoy.setPattern(".*\\|reversed.*");
}

PeptideResultsQvalueHandler::~PeptideResultsQvalueHandler()
{
}

bool
PeptideResultsQvalueHandler::startElement(const QString &namespaceURI,
                                          const QString &localName,
                                          const QString &qName,
                                          const QXmlAttributes &attributes)
{

  // qDebug() << namespaceURI << " " << localName << " " << qName ;
  _tag_stack.push_back(qName);
  bool is_ok = true;

  try
    {
      // startElement_group
      if(qName == "scan")
        {
          is_ok = startElement_scan(attributes);
        }
      else if(qName == "psm")
        {
          is_ok = startElement_psm(attributes);
        }

      _current_text.clear();
    }
  catch(pappso::PappsoException &exception_pappso)
    {
      _errorStr = QObject::tr(
                    "ERROR in PeptideResultsQvalueHandler::startElement tag "
                    "%1, PAPPSO exception:\n%2")
                    .arg(qName)
                    .arg(exception_pappso.qwhat());
      return false;
    }
  catch(std::exception &exception_std)
    {
      _errorStr = QObject::tr(
                    "ERROR in PeptideResultsQvalueHandler::startElement tag "
                    "%1, std exception:\n%2")
                    .arg(qName)
                    .arg(exception_std.what());
      return false;
    }
  return is_ok;
}

bool
PeptideResultsQvalueHandler::endElement(const QString &namespaceURI,
                                        const QString &localName,
                                        const QString &qName)
{

  bool is_ok = true;
  // endElement_peptide_list
  try
    {
      if(qName == "scan")
        {
          is_ok = endElement_scan();
        }
    }
  catch(pappso::PappsoException &exception_pappso)
    {
      _errorStr = QObject::tr(
                    "ERROR in PeptideResultsQvalueHandler::endElement tag %1, "
                    "PAPPSO exception:\n%2")
                    .arg(qName)
                    .arg(exception_pappso.qwhat());
      return false;
    }
  catch(std::exception &exception_std)
    {
      _errorStr = QObject::tr(
                    "ERROR in PeptideResultsQvalueHandler::endElement tag %1, "
                    "std exception:\n%2")
                    .arg(qName)
                    .arg(exception_std.what());
      return false;
    }

  _current_text.clear();
  _tag_stack.pop_back();

  return is_ok;
}

bool
PeptideResultsQvalueHandler::startDocument()
{

  return true;
}

bool
PeptideResultsQvalueHandler::endDocument()
{


  std::sort(m_scoreList.begin(),
            m_scoreList.end(),
            [](const PsmScore &scorea, PsmScore &scoreb) {
              return (scorea.m_evalue < scoreb.m_evalue);
            });
  m_countTarget = 0;
  m_countDecoy  = 0;
  for(auto &score : m_scoreList)
    {
      if(score.m_isDecoy)
        {
          m_countDecoy++;
        }
      else
        {
          m_countTarget++;
        }
      score.m_qvalue = (double)m_countDecoy / (double)m_countTarget;

      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
               << " score.m_qvalue =" << score.m_qvalue;
    }

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  // check reverse list to clean q-values
  auto rit = m_scoreList.rbegin();

  double qvalue_max = 99999999;
  while(rit != m_scoreList.rend())
    {
      if(rit->m_qvalue > qvalue_max)
        {
          rit->m_qvalue = qvalue_max;
        }
      qvalue_max = rit->m_qvalue;
      rit++;
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " count_target=" << m_countTarget
           << " count_decoy=" << m_countDecoy
           << " m_scoreList.size()=" << m_scoreList.size();
  return true;
}

bool
PeptideResultsQvalueHandler::characters(const QString &str)
{
  _current_text += str;
  return true;
}


bool
PeptideResultsQvalueHandler::error(const QXmlParseException &exception)
{
  _errorStr = QObject::tr(
                "Parse error at line %1, column %2 :\n"
                "%3")
                .arg(exception.lineNumber())
                .arg(exception.columnNumber())
                .arg(exception.message());

  return false;
}


bool
PeptideResultsQvalueHandler::fatalError(const QXmlParseException &exception)
{
  _errorStr = QObject::tr(
                "Parse error at line %1, column %2 :\n"
                "%3")
                .arg(exception.lineNumber())
                .arg(exception.columnNumber())
                .arg(exception.message());
  return false;
}

QString
PeptideResultsQvalueHandler::errorString() const
{
  return _errorStr;
}


bool
PeptideResultsQvalueHandler::startElement_scan(QXmlAttributes attrs)
{
  bool is_ok = true;
  m_oneScanScoreList.clear();

  return is_ok;
}

bool
PeptideResultsQvalueHandler::startElement_psm(QXmlAttributes attributes)
{
  QString psm_key(QString("%1-%2")
                    .arg(attributes.value("seq"))
                    .arg(attributes.value("mhTheo")));
  auto ret = m_oneScanScoreList.insert(
    std::pair<QString, PsmScore>(psm_key, PsmScore()));

  ret.first->second.m_evalue = attributes.value("evalue").toDouble();
  //|reversed

  if(m_regexpDecoy.match(attributes.value("prot")).hasMatch())
    {
      ret.first->second.m_isDecoy = true;

      qDebug() << " attributes.value(prot=" << attributes.value("prot");
      qDebug() << " ret.first->second.m_isDecoy ="
               << ret.first->second.m_isDecoy;
    }

  return true;
}


bool
PeptideResultsQvalueHandler::endElement_scan()
{
  bool is_ok = true;
  for(auto &pair_scan_score : m_oneScanScoreList)
    {
      m_scoreList.push_back(pair_scan_score.second);
    }
  return is_ok;
}

double
PeptideResultsQvalueHandler::getEvalueThresholdForFdr(double target_fdr) const
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " target_fdr=" << target_fdr;

  auto rit = m_scoreList.rbegin();

  PsmScore previous = *rit;
  while(rit != m_scoreList.rend())
    {
      if(target_fdr > rit->m_qvalue)
        {
          return previous.m_evalue;
        }
      previous = *rit;
      rit++;
    }
  return 0;
}

void
PeptideResultsQvalueHandler::setDecoyRegexp(const QString &decoyOptionStr)
{
  m_regexpDecoy.setPattern(decoyOptionStr);
}


std::size_t
PeptideResultsQvalueHandler::getCountDecoy() const
{
  return m_countDecoy;
}
std::size_t
PeptideResultsQvalueHandler::getCountTarget() const
{
  return m_countTarget;
}
