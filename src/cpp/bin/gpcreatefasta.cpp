/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of Protein Grouper.
 *
 *     Protein Grouper is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Protein Grouper is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Protein Grouper.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include <QString>
#include <QLocale>
#include <QDateTime>
#include <QDir>
#include <QTimer>
#include <QCommandLineParser>
#include <iostream>
#include <pappsomspp/pappsoexception.h>
#include "../core/gp_lib_config.h"
#include "gpcreatefasta.h"
#include "../core/gp_error.h"
#include "../input/peptideresultsngreader.h"
#include <pappsomspp/fasta/fastareader.h>
#include <pappsomspp/fasta/fastaoutputstream.h>

class FastaExtractor : public pappso::FastaHandlerInterface
{
  public:
  FastaExtractor(pappso::FastaWriterInterface &output,
                 const std::set<QString> &accession_list)
    : _output(output)
  {
    m_accessionList = accession_list;
  };

  void
  setSequence(const QString &description, const QString &sequence) override
  {
    qDebug() << "FastaRenamer::setSequence " << description << " " << sequence;
    pappso::Protein protein(description, sequence);
    if(m_accessionList.contains(protein.getAccession()))
      {
        _output.writeProtein(protein);
      }
  };

  private:
  std::set<QString> m_accessionList;
  pappso::FastaWriterInterface &_output;
};


GpCreateFasta::GpCreateFasta(QObject *parent) : QObject(parent)
{
  // get the instance of the main application
  app = QCoreApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}

GpCreateFasta::~GpCreateFasta()
{
}

// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
GpCreateFasta::run()
{
  // ./src/cpp/bin/gp-output-proticdbml -i
  // '/gorgone/pappso/versions_logiciels_pappso/gp/groups.xml' -f
  // /gorgone/pappso/moulon/users/thierry/amaizing/reinterrogation_xtandem_point_mut_isotopes_271115/protic/amaizing_database.fasta
  // -o /gorgone/pappso/versions_logiciels_pappso/gp/protic.xml


  //./src/pt-fastarenamer -i
  /// gorgone/pappso/jouy/database/Strepto_Aaron/20151106_grouping_ortho_uniprot_TK24_M145.fasta
  //-c
  /// gorgone/pappso/jouy/database/Strepto_Aaron/20151106_grouping_TK24_M145.ods
  //-o /tmp/test.fasta

  QTextStream errorStream(stderr, QIODevice::WriteOnly);
  QTextStream outStream(stdout, QIODevice::WriteOnly);

  try
    {
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      QCommandLineParser parser;

      // throw pappso::PappsoException("test");
      parser.setApplicationDescription(
        QString("gp-create-fasta")
          .append(" ")
          .append(GP_VERSION)
          .append(" extract protein and create new fasta file"));
      parser.addHelpOption();
      parser.addVersionOption();

      QCommandLineOption inputOption(
        QStringList() << "x"
                      << "xml",
        QCoreApplication::translate(
          "main", "XML peptide identification result file <peptides>."),
        QCoreApplication::translate("main", "XML input file"));

      QCommandLineOption inputFastaOption(
        QStringList() << "f"
                      << "fasta",
        QCoreApplication::translate("main",
                                    "input reference FASTA file <fasta>."),
        QCoreApplication::translate("main", "input FASTA file"));

      QCommandLineOption outputFastaOption(
        QStringList() << "o",
        QCoreApplication::translate("main", "output FASTA file <output>."),
        QCoreApplication::translate("main", "output FASTA file"),
        QString());

      parser.addOption(inputOption);
      parser.addOption(inputFastaOption);
      parser.addOption(outputFastaOption);

      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

      const QDateTime dt_begin = QDateTime::currentDateTime();
      const QStringList args   = parser.positionalArguments();


      QString peptidesFileStr = parser.value(inputOption);
      QString inputFastaStr   = parser.value(inputFastaOption);
      QString outputFastaStr  = parser.value(outputFastaOption);


      PeptideResultsNgReader parser_peptide;
      qDebug() << peptidesFileStr;
      if(parser_peptide.readFile(peptidesFileStr))
        {
        }
      else
        {
          throw pappso::PappsoException(
            QString("error %1").arg(parser_peptide.errorString()));
        }


      QFile fastaFile;
      fastaFile.setFileName(inputFastaStr);
      if(!fastaFile.open(QIODevice::ReadOnly))
        {
          throw pappso::PappsoException(
            QObject::tr("ERROR: unable to open %1 FASTA file for read")
              .arg(inputFastaStr));
        }


      QTextStream *p_outputStream;
      QFile outFile;
      outFile.setFileName(outputFastaStr);
      if(!outFile.open(QIODevice::WriteOnly))
        {
          throw pappso::PappsoException(
            QObject::tr("ERROR: unable to open %1 output file for write")
              .arg(outputFastaStr));
        }
      p_outputStream = new QTextStream(&outFile);


      pappso::FastaOutputStream fasta_output(*p_outputStream);


      qDebug() << "renamer(fasta_output, map_ods.getMap(), include_all)";
      FastaExtractor renamer(fasta_output, parser_peptide.getProteinList());
      pappso::FastaReader reader(renamer);
      reader.parse(&fastaFile);
      fastaFile.close();

      p_outputStream->flush();
      delete p_outputStream;
      qDebug();
    }
  catch(GpError &gperror)
    {
      errorStream << "Oops! an error occurred in GP. Dont Panic :" << Qt::endl;
      errorStream << gperror.qwhat() << Qt::endl;
      exit(1);
      app->exit(1);
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in GP. Dont Panic :" << Qt::endl;
      errorStream << error.qwhat() << Qt::endl;
      exit(1);
      app->exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in GP. Dont Panic :" << Qt::endl;
      errorStream << error.what() << Qt::endl;
      exit(1);
      app->exit(1);
    }


  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
GpCreateFasta::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
GpCreateFasta::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));
  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  QCoreApplication app(argc, argv);
  QCoreApplication::setApplicationName("gp-create-fasta");
  QCoreApplication::setApplicationVersion(GP_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  GpCreateFasta myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(
    &app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
