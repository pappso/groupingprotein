/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of Protein Grouper.
 *
 *     Protein Grouper is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Protein Grouper is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Protein Grouper.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include <QString>
#include <QLocale>
#include <QDateTime>
#include <QDir>
#include <QTimer>
#include <QCommandLineParser>
#include <iostream>
#include <cstdio>
#include <pappsomspp/pappsoexception.h>
#include "../core/gp_lib_config.h"
#include "../core/gp_engine.h"
#include "../core/gp_params.h"
#include "../core/gp_error.h"
#include "../core/gp_engine.h"
#include "gpoutputproticdbml.h"
#include "../output/proticdbml.h"
#include "../input/groupinghandler.h"


GpOutputProticdbml::GpOutputProticdbml(QObject *parent) : QObject(parent)
{
  // get the instance of the main application
  app = QCoreApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}

// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
GpOutputProticdbml::run()
{
  // ./src/cpp/bin/gp-output-proticdbml -i
  // '/gorgone/pappso/versions_logiciels_pappso/gp/groups.xml' -f
  // /gorgone/pappso/moulon/users/thierry/amaizing/reinterrogation_xtandem_point_mut_isotopes_271115/protic/amaizing_database.fasta
  // -o /gorgone/pappso/versions_logiciels_pappso/gp/protic.xml


  //./src/pt-fastarenamer -i
  /// gorgone/pappso/jouy/database/Strepto_Aaron/20151106_grouping_ortho_uniprot_TK24_M145.fasta
  //-c
  /// gorgone/pappso/jouy/database/Strepto_Aaron/20151106_grouping_TK24_M145.ods
  //-o /tmp/test.fasta

  QTextStream errorStream(stderr, QIODevice::WriteOnly);

  try
    {
      qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      QCommandLineParser parser;

      // throw pappso::PappsoException("test");
      parser.setApplicationDescription(QString("gp-output-proticdbml")
                                         .append(" ")
                                         .append(GP_VERSION)
                                         .append(" PROTICdbML writer"));
      parser.addHelpOption();
      parser.addVersionOption();
      QCommandLineOption inputOption(
        QStringList() << "i"
                      << "grouping",
        QCoreApplication::translate(
          "main", "protein grouper grouping XML result file <grouping>."),
        QCoreApplication::translate("main", "input"));

      QCommandLineOption outputOption(
        QStringList() << "o"
                      << "proticdbml",
        QCoreApplication::translate("main",
                                    "Write PROTICdbML output file <output>."),
        QCoreApplication::translate("main", "output"));


      QCommandLineOption fastaOption(
        QStringList() << "f"
                      << "fasta",
        QCoreApplication::translate("main",
                                    "FASTA file containing sequences <fasta>."),
        QCoreApplication::translate("main", "fasta"));

      parser.addOption(outputOption);
      parser.addOption(inputOption);
      parser.addOption(fastaOption);

      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

      const QDateTime dt_begin = QDateTime::currentDateTime();
      const QStringList args   = parser.positionalArguments();


      QString groupingFileStr = parser.value(inputOption);
      QString fastaFileStr    = parser.value(fastaOption);


      QString proticFileStr = parser.value(outputOption);
      if(!proticFileStr.isEmpty())
        {
          ProticdbMl output(proticFileStr);


          GroupingHandler parser(&output, fastaFileStr);
          qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
          QXmlSimpleReader simplereader;
          simplereader.setContentHandler(&parser);
          simplereader.setErrorHandler(&parser);

          if(groupingFileStr.isEmpty())
            {
              QTextStream *p_in = new QTextStream(stdin, QIODevice::ReadOnly);
              QXmlInputSource xmlInputSource(p_in->device());

              if(simplereader.parse(xmlInputSource))
                {
                }
              else
                {
                  throw GpError(parser.errorString());
                }
            }
          else
            {
              QFile grouping_file(groupingFileStr);
              QXmlInputSource xmlInputSource(&grouping_file);

              if(simplereader.parse(xmlInputSource))
                {
                }
              else
                {
                  throw GpError(parser.errorString());
                }
            }
        }
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
    }
  catch(GpError &gperror)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << Qt::endl;
      errorStream << gperror.qwhat() << Qt::endl;
      exit(1);
      app->exit(1);
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << Qt::endl;
      errorStream << error.qwhat() << Qt::endl;
      exit(1);
      app->exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << Qt::endl;
      errorStream << error.what() << Qt::endl;
      exit(1);
      app->exit(1);
    }


  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
GpOutputProticdbml::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
GpOutputProticdbml::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{
  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  QCoreApplication app(argc, argv);
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  QCoreApplication::setApplicationName("gp-output-proticdbml");
  QCoreApplication::setApplicationVersion(GP_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  GpOutputProticdbml myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(
    &app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
