/*******************************************************************************
 * Copyright (c) 2025 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of Protein Grouper.
 *
 *     Protein Grouper is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Protein Grouper is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Protein Grouper.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include <QString>
#include <QLocale>
#include <QDateTime>
#include <QDir>
#include <QTimer>
#include <QCommandLineParser>
#include <iostream>
#include <cstdio>
#include <pappsomspp/pappsoexception.h>
#include "../core/gp_lib_config.h"
#include "../core/gp_engine.h"
#include "../core/gp_params.h"
#include "../core/gp_error.h"
#include "../core/gp_engine.h"
#include "gpoutputmasschroq.h"
#include "../input/groupinghandlermasschroq.h"
#include <pappsomspp/processing/uimonitor/uimonitortextpercent.h>
#include <sys/stat.h>


GpOutputMassChroq::GpOutputMassChroq(QObject *parent) : QObject(parent)
{
  // get the instance of the main application
  app = QCoreApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}

// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
GpOutputMassChroq::run()
{
  // ./src/cpp/bin/gp-output-proticdbml -i
  // '/gorgone/pappso/versions_logiciels_pappso/gp/groups.xml' -f
  // /gorgone/pappso/moulon/users/thierry/amaizing/reinterrogation_xtandem_point_mut_isotopes_271115/protic/amaizing_database.fasta
  // -o /gorgone/pappso/versions_logiciels_pappso/gp/protic.xml


  //./src/pt-fastarenamer -i
  /// gorgone/pappso/jouy/database/Strepto_Aaron/20151106_grouping_ortho_uniprot_TK24_M145.fasta
  //-c
  /// gorgone/pappso/jouy/database/Strepto_Aaron/20151106_grouping_TK24_M145.ods
  //-o /tmp/test.fasta

  QTextStream errorStream(stderr, QIODevice::WriteOnly);

  pappso::UiMonitorTextPercent ui_monitor(errorStream);


  try
    {
      qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      QCommandLineParser parser;

      // throw pappso::PappsoException("test");
      parser.setApplicationDescription(QString("gp-output-proticdbml")
                                         .append(" ")
                                         .append(GP_VERSION)
                                         .append(" PROTICdbML writer"));
      parser.addHelpOption();
      parser.addVersionOption();
      QCommandLineOption inputOption(
        QStringList() << "i"
                      << "grouping",
        QCoreApplication::translate(
          "main", "protein grouper grouping XML result file <grouping>."),
        QCoreApplication::translate("main", "input"));

      QCommandLineOption outputOption(
        QStringList() << "o"
                      << "json",
        QCoreApplication::translate("main",
                                    "Write MassChroQ JSON output file <json>."),
        QCoreApplication::translate("main", "json"));


      parser.addOption(outputOption);
      parser.addOption(inputOption);


      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

      const QDateTime dt_begin = QDateTime::currentDateTime();
      const QStringList args   = parser.positionalArguments();


      QString groupingFileStr = parser.value(inputOption);

      QString jsonFileStr = parser.value(outputOption);


      GroupingHandlerMassChroq parser_gp;

      QFile file(QFileInfo(groupingFileStr).absoluteFilePath());
      if(groupingFileStr == "-")
        {
          file.open(stdin, QIODevice::ReadOnly);
        }
      else
        {
          if(!file.open(QIODevice::ReadOnly))
            {
              throw pappso::PappsoException(
                QObject::tr("Unable to open xml grouping file '%1' :\n%2\n")
                  .arg(QFileInfo(groupingFileStr).absoluteFilePath())
                  .arg(file.errorString()));
            }
          file.open(QIODevice::ReadOnly);


          ui_monitor.setStatus(
            QObject::tr("Parsing XML input file '%1'")
              .arg(QFileInfo(groupingFileStr).absoluteFilePath()));
        }


      if(parser_gp.read(&file))
        {
          file.close();
          QJsonDocument doc;

          parser_gp.populateJsonDocument(doc);


          QFile jsonf(jsonFileStr);
          if(jsonFileStr == "-" ? jsonf.open(stdout, QIODevice::WriteOnly)
                                : jsonf.open(QIODevice::WriteOnly))
            {
            }
          jsonf.write(doc.toJson());
          jsonf.close();
        }
      else
        {
          file.close();
          throw pappso::PappsoException(
            QObject::tr("error reading grouping XML input file '%1' :\n")
              .arg(QFileInfo(groupingFileStr).absoluteFilePath())
              .append(parser_gp.errorString()));
        }
    }
  catch(GpError &gperror)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << Qt::endl;
      errorStream << gperror.qwhat() << Qt::endl;
      exit(1);
      app->exit(1);
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << Qt::endl;
      errorStream << error.qwhat() << Qt::endl;
      exit(1);
      app->exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << Qt::endl;
      errorStream << error.what() << Qt::endl;
      exit(1);
      app->exit(1);
    }


  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
GpOutputMassChroq::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
GpOutputMassChroq::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{
  // QTextStream consoleErr(stderr);
  umask(0);
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  QCoreApplication app(argc, argv);
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  QCoreApplication::setApplicationName("gp-output-proticdbml");
  QCoreApplication::setApplicationVersion(GP_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  GpOutputMassChroq myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(
    &app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
